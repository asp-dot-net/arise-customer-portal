﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class Utility
    {
        public string OrganizationName { get; set; }

        public string Logo { get; set; }

        public string TelPhone { get; set; }
    }
}
