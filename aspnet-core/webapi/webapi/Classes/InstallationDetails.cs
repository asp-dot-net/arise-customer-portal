﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class InstallationDetails
    {
        public Decimal? PaymentDue { get; set; }

        public DateTime? InstallationBookingDate { get; set; }

        public DateTime? InstallationStatus { get; set; }

        public string NetMeterStatus { get; set; }
    }
}
