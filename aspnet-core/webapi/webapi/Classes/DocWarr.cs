﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class DocWarr
    {
        public string ComplianceCertificate { get; set; }

        public string SignQuotation { get; set; }

        public string GridConnectionApproval { get; set; }

        public string PanelLogo { get; set; }

        public string PanelName { get; set; }
        
        public string InverterLogo { get; set; }

        public string InverterName { get; set; }
    }
}
