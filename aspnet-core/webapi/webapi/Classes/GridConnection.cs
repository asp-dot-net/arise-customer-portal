﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class GridConnection
    {
        public DateTime? MeterApplicationDate { get; set; }

        public DateTime? MeterApplicationApprovedDate { get; set; }

        public string NMINumber { get; set; }

        public string EnergyProvider { get; set; }

    }
}
