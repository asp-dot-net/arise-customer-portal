﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class OrderDetails
    {
        public string QuoteLink { get; set; }

        public string ProjectNo { get; set; }

        public string RoofType { get; set; }

        public string RoofAngle { get; set; }

        public string BuildingType { get; set; }
        
        public string MeterPhase { get; set; }
        
        public string NMINumber { get; set; }
        
        public string InstallationType { get; set; }
        
        public Decimal? PaymentDue { get; set; }

        public List<ProductItemDto> ProductItems { get; set; }
    }

    public class ProductItemDto
    {
        public string ProductType { get; set; }

        public string Name { get; set; }

        public int? Quantity { get; set; }
    }
}
