﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class Dashboard
    {
        public string CustomerName { get; set; }

        public string ProjectNo { get; set; }

        public decimal? SystemKw { get; set; }

        public string OrganizationName { get; set; }

        public string QuoteLink { get; set; }

        public bool OrderDettail { get; set; }

        public bool DocumentSubmission { get; set; }
        
        public bool GridConnApp { get; set; }
        
        public bool InstallationDetails { get; set; }

        public bool DocWarr { get; set; }
    }
}
