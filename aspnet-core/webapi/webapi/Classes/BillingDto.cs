﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class BillingDto
    {
        public string ProjectNo { get; set; }

        public string CustomerName { get; set; }

        public decimal? PaymentDue { get; set; }

        public string InvoicePdf { get; set; }

        public List<InvoiceDto> Invoices { get; set; }
    }

    public class InvoiceDto
    {
        public DateTime? Date { get; set; }

        public string PaymentMethod { get; set; }

        public string Description { get; set; }

        public decimal? Amount { get; set; }

        public string Receipt { get; set; }
    }
}
