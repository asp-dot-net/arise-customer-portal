﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Classes
{
    public class RefereFriendDto
    {
        public DateTime? RefferalDate { get; set; }

        public string Name { get; set; }

        public DateTime? ContactedDate { get; set; }

        public string ProjectNo { get; set; }

        public decimal SystemSize { get; set; }

        public string BalanceOwing { get; set; }

        public string ReferralAmount { get; set; }

        public DateTime? PaidDate { get; set; }
    }

    public class RefereFriendInputDto
    {
        public int JonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string PropertyAddress { get; set; }
    }
}
