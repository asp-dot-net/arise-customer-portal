﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Classes;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/utility")]
    [ApiController]
    [Authorize]
    public class UtilityController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public UtilityController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<Utility> GetUtility()
        {
            Utility utility = new Utility();

            try
            {
                //int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);
                int OrganizationId = Convert.ToInt32(User.Claims.Where(c => c.Type == "OrganizationId").First().Value);

                var query = from org in _dbContext.AbpOrganizationUnits
                            where org.Id == OrganizationId
                            select new Utility
                            {
                                OrganizationName = org.DefaultFromDisplayName,
                                Logo = org.LogoFilePath,
                                TelPhone = org.Mobile
                            };

                utility = query.FirstOrDefault();

                await Task.CompletedTask;
            }
            catch
            { }

            return utility;
        }
    }
}
