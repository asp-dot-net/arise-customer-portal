﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Classes;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/billing")]
    [ApiController]
    [Authorize]
    public class BillingController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public BillingController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<BillingDto> GetAsync()
        {
            BillingDto billingDto = new BillingDto();
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                billingDto = _dbContext.Jobs.Where(x => x.Id == jobId).Select(x =>
                new BillingDto
                {
                    ProjectNo = x.JobNumber,
                    CustomerName = x.Lead.CompanyName,
                    PaymentDue = x.TotalCost - x.InvoiceImportData.Where(x => x.IsDeleted == false).Sum(x => x.PaidAmmount),

                    Invoices = x.InvoiceImportData.Where(x => x.IsDeleted == false).OrderByDescending(x => x.CreationTime).Select(x => new InvoiceDto
                    {
                        Date = x.Date,
                        PaymentMethod = x.InvoicePaymentMethod.ShortCode,
                        Description = x.Description,
                        Amount = x.PaidAmmount

                    }).ToList()

                }).First();

                await Task.CompletedTask;
            }
            catch
            { }

            return billingDto;
        }
    }
}
