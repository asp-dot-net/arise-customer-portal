﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using webapi.Classes;
using webapi.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;

namespace webapi.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        private readonly ApplicationSettings _appSettings;

        public AuthController(DatabaseDbContext dbContext, IOptions<ApplicationSettings> appSetting)
        {
            _dbContext = dbContext;
            _appSettings = appSetting.Value;
        }

        [HttpPost]
        [Route("login")] // POST: /api/auth/login
        public async Task<IActionResult> Login(LoginModel login)
        {
            try
            {
                var _job = _dbContext.Jobs.Where(job => job.JobNumber == login.ProjectNo).Select(x => new { x.LeadId, x.Id }).First();

                var _email = _dbContext.Leads.Where(lead => lead.Id == _job.LeadId && lead.Email == login.Email).Select(x => new { x.Id, x.OrganizationId }).First();

                //var _job = from job in _dbContext.Jobs where job.JobNumber == login.ProjectNo select new { job.Id, job.LeadId };
                //var _email = from lead in _dbContext.Leads where lead.Id == _job.First().LeadId && lead.Email == login.Email select new { lead.OrganizationId };
                if (_email != null && _job != null)
                {
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                        new Claim("JobId", _job.Id.ToString()),
                        new Claim("LeadId", _job.LeadId.ToString()),
                        new Claim("OrganizationId", _email.OrganizationId.ToString())
                        }),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256Signature),
                        Issuer = _appSettings.Server_URL.ToString(),
                        Audience = _appSettings.Client_URL.ToString()
                    };

                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                    var token = tokenHandler.WriteToken(securityToken);

                    await Task.CompletedTask;
                    return Ok(new { token, isAuthSuccessful = true });
                }
                else
                {
                    //return BadRequest(new { message = "Authentication failed.." });
                    return Unauthorized();
                }
            }
            catch
            {
                return Unauthorized();
            }
        }

    }
}
