﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Classes;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/installationdetails")]
    [ApiController]
    [Authorize]
    public class InstallationDetailsController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public InstallationDetailsController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<InstallationDetails> GetAsync()
        {
            InstallationDetails installationDetails  = new InstallationDetails();
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                //var query = from job in _dbContext.Jobs
                //            join rt in _dbContext.RoofTypes
                //            on job.RoofType equals lead.Id
                //            join org in _dbContext.AbpOrganizationUnits
                //            on lead.OrganizationId equals org.Id
                //            where job.Id == jobId
                //            select new OrderDetails
                //            {
                //                ProjectNo = job.JobNumber
                //                //RoofType = job.ro,
                //                //SystemKw = job.SystemCapacity,
                //                //OrganizationName = org.DefaultFromDisplayName
                //            };

                installationDetails = _dbContext.Jobs.Where(x => x.Id == jobId).Select(x =>
                new InstallationDetails
                {
                    PaymentDue = x.TotalCost - x.InvoiceImportData.Where(x => x.IsDeleted == false).Sum(x => x.PaidAmmount),
                    InstallationBookingDate = x.InstallationDate,
                    InstallationStatus = x.InstalledcompleteDate,
                    NetMeterStatus = x.MeterApplyRef
                }).First();

                await Task.CompletedTask;
            }
            catch
            { }

            return installationDetails;
        }
    }
}
