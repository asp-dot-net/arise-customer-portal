﻿using EmailService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/forgotprojectno")]
    [ApiController]
    public class ForgotProjectNo : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;

        private readonly IEmailSender _emailSender;

        public ForgotProjectNo(DatabaseDbContext dbContext, IEmailSender emailSender)
        {
            _dbContext = dbContext;
            _emailSender = emailSender;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(string Email)
        {
            try
            {
                var existEmail = _dbContext.Leads.Where(x => x.Email == Email).Select(x => x.OrganizationId);
                if (existEmail.Count() == 0)
                {
                    await Task.CompletedTask;
                    return Ok(new { StatusCode = StatusCodes.Status404NotFound, message = "Your email does not exists, Please contact your SalesRep." });
                }

                var Jobs = _dbContext.Jobs.Where(x => x.Lead.Email == Email && x.DepositeRecceivedDate != null).Select(x => x.JobNumber).ToList();
                if (Jobs.Count() == 0)
                {
                    await Task.CompletedTask;
                    return Ok(new { StatusCode = StatusCodes.Status404NotFound, message = "Your email does not have any job or not received deposit." });
                }
                else
                {
                    //var message = msg(existEmail.FirstOrDefault(), Jobs, Email);
                    string pNo = "";
                    for (int i = 0; i < Jobs.Count; i++)
                    {
                        pNo += "," + Jobs[i];
                    }
                    string From = "";
                    string Pwd = "";
                    if (existEmail.FirstOrDefault() == 1)
                    {
                        From = "info@solarbridge.com.au";
                        Pwd = "@@Bridge@123";
                    }
                    else if (existEmail.FirstOrDefault() == 2)
                    {
                        From = "info@savvysolar.com.au";
                        Pwd = "Tas@321#";
                    }

                    var content = "Your Project No is: " + pNo.Substring(1);

                    string[] _email = { Email };

                    var message = new Message(From, Pwd, _email, "Your Project No", content, null);

                    await _emailSender.SendEmailAsync(message);

                    await Task.CompletedTask;
                    return Ok(new { StatusCode = StatusCodes.Status200OK, message = "Send your project no in your register email address." });
                }

                //return Ok(new { status = StatusCodes.Status200OK });
            }
            catch
            {
                //return BadRequest(new { message = "Authentication failed.." });
                return BadRequest();
            }
        }
    }
}
