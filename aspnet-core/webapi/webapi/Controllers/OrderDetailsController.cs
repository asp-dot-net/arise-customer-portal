﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Classes;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/orderdetails")]
    [ApiController]
    [Authorize]
    public class OrderDetailsController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public OrderDetailsController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<OrderDetails> GetAsync()
        {
            OrderDetails orderDetails = new OrderDetails();
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                //var query = from job in _dbContext.Jobs
                //            join rt in _dbContext.RoofTypes
                //            on job.RoofType equals lead.Id
                //            join org in _dbContext.AbpOrganizationUnits
                //            on lead.OrganizationId equals org.Id
                //            where job.Id == jobId
                //            select new OrderDetails
                //            {
                //                ProjectNo = job.JobNumber
                //                //RoofType = job.ro,
                //                //SystemKw = job.SystemCapacity,
                //                //OrganizationName = org.DefaultFromDisplayName
                //            };

                orderDetails = _dbContext.Jobs.Where(x => x.Id == jobId).Select(x =>
                new OrderDetails
                {
                    ProjectNo = x.JobNumber,
                    RoofType = x.RoofType.Name,
                    RoofAngle = x.RoofAngle.Name,
                    BuildingType = x.HouseType.Name,
                    MeterPhase = x.MeterPhaseId.ToString(),
                    NMINumber = x.Nminumber,
                    InstallationType = x.JobType.Name,
                    QuoteLink = x.Quotations.Where(x => x.IsDeleted == false).Select(x => new { x.QuoteFileName, x.QuoteFilePath, x.CreationTime }).OrderByDescending(x => x.CreationTime).Select(x => x.QuoteFilePath + x.QuoteFileName).First(),
                    PaymentDue = x.TotalCost - x.InvoiceImportData.Where(x => x.IsDeleted == false).Sum(x => x.PaidAmmount),

                    ProductItems = x.JobProductItems.Where(x => x.IsDeleted == false).OrderBy(_ => _.ProductItem.ProductTypeId).Select(x => new ProductItemDto { ProductType = x.ProductItem.ProductType.Name, Name = x.ProductItem.Name, Quantity = x.Quantity }).ToList()

                }).First();

                await Task.CompletedTask;
            }
            catch
            { }

            return orderDetails;
        }
    }
}
