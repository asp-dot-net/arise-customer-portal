﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;
using webapi.Classes;

namespace webapi.Controllers
{
    [Route("api/dashboard")]
    [ApiController]
    [Authorize]
    public class DashboardController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public DashboardController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<Dashboard> GetDashboard()
        {
            Dashboard dashboard = new Dashboard();
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                dashboard = _dbContext.Jobs.Where(x => x.Id == jobId).Select(x => 
                new Dashboard
                {
                    ProjectNo = x.JobNumber,
                    CustomerName = x.Lead.CompanyName,
                    SystemKw = x.SystemCapacity,
                    OrganizationName = _dbContext.AbpOrganizationUnits.Where(o => o.Id == x.LeadId).First().DefaultFromDisplayName,
                    QuoteLink = x.Quotations.Where(x => x.IsDeleted == false).Select(x => new { x.QuoteFileName, x.QuoteFilePath, x.CreationTime }).OrderByDescending(x => x.CreationTime).Select(x => x.QuoteFilePath + x.QuoteFileName).First(),
                    OrderDettail = string.IsNullOrEmpty(x.RoofType.Name) || string.IsNullOrEmpty(x.RoofAngle.Name) || string.IsNullOrEmpty(x.HouseType.Name) || string.IsNullOrEmpty(x.MeterPhaseId.ToString()) || string.IsNullOrEmpty(x.Nminumber) || string.IsNullOrEmpty(x.JobType.Name) || string.IsNullOrEmpty(x.JobProductItems.Where(x => x.IsDeleted == false).Select(x => new { x.Quantity, x.ProductItem.Name, x.ProductItem.ProductType.Id }).Where(x => x.Id == 1).Select(x => x.Quantity + " x " + x.Name).First()) || string.IsNullOrEmpty(x.Quotations.Where(x => x.IsDeleted == false).Select(x => new { x.QuoteFileName, x.QuoteFilePath, x.CreationTime }).OrderByDescending(x => x.CreationTime).Select(x => x.QuoteFilePath + x.QuoteFileName).First()) ? false : true,
                    GridConnApp = string.IsNullOrEmpty(x.InspectionDate.ToString()) || string.IsNullOrEmpty(x.Nminumber) || string.IsNullOrEmpty(x.ElecRetailer.Name)  ? false : true,
                    InstallationDetails = string.IsNullOrEmpty(x.InstallationDate.ToString()) || string.IsNullOrEmpty(x.InstalledcompleteDate.ToString()) || string.IsNullOrEmpty(x.MeterApplyRef) ? false : true
                }).First();

                await Task.CompletedTask;
            }
            catch
            { }

            return dashboard;
        }
    }
}
