﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;
using webapi.Classes;

namespace webapi.Controllers
{
    [Route("api/gridconnection")]
    [ApiController]
    [Authorize]
    public class GridConnectionController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public GridConnectionController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<GridConnection> GetAsync()
        {
            GridConnection gridConnection = new GridConnection();
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                //var query = from job in _dbContext.Jobs
                //            join rt in _dbContext.RoofTypes
                //            on job.RoofType equals lead.Id
                //            join org in _dbContext.AbpOrganizationUnits
                //            on lead.OrganizationId equals org.Id
                //            where job.Id == jobId
                //            select new OrderDetails
                //            {
                //                ProjectNo = job.JobNumber
                //                //RoofType = job.ro,
                //                //SystemKw = job.SystemCapacity,
                //                //OrganizationName = org.DefaultFromDisplayName
                //            };

                gridConnection = _dbContext.Jobs.Where(x => x.Id == jobId).Select(x =>
                new GridConnection
                {
                    MeterApplicationDate = x.InspectionDate,
                    MeterApplicationApprovedDate = x.InspectionDate,
                    NMINumber = x.Nminumber,
                    EnergyProvider = x.ElecRetailer.Name
                }).First();

                await Task.CompletedTask;
            }
            catch
            { }

            return gridConnection;
        }
    }
}
