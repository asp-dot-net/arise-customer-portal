﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Classes;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/referefriend")]
    [ApiController]
    [Authorize]
    public class RefereFriendController : ControllerBase
    {
        private readonly DatabaseDbContext _dbContext;
        public RefereFriendController(DatabaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(RefereFriendInputDto input)
        {
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                CpReferelDetail details = new CpReferelDetail
                {
                    JobId = jobId,
                    FirstName = input.FirstName,
                    LastName = input.LastName,
                    Email = input.Email,
                    Mobile = input.LastName,
                    PropertyAddress = input.PropertyAddress,
                    CreatedDate = System.DateTime.Now.AddHours(15)
                };

                await _dbContext.CpReferelDetails.AddAsync(details);
                _dbContext.SaveChanges();

                return Ok(new { status = StatusCodes.Status200OK });
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<List<RefereFriendDto>> GetAsync()
        {
            List<RefereFriendDto> refereFriendDto = new List<RefereFriendDto>();
            try
            {
                int jobId = Convert.ToInt32(User.Claims.First(c => c.Type == "JobId").Value);

                refereFriendDto = _dbContext.CpReferelDetails.Where(x => x.JobId == jobId).Select(x =>
                new RefereFriendDto
                {
                    RefferalDate = x.CreatedDate,
                    Name = x.FirstName + " " + x.LastName,
                }).ToList();

                //var query = from r in _dbContext.CpReferelDetails
                //            join l in _dbContext.Leads
                //            on r.Email equals l.Email
                //            where r.JobId == jobId
                //            select new RefereFriendDto
                //            {
                //                Name = r.FirstName + " " + r.LastName,
                //                RefferalDate = r.CreatedDate,
                //                ProjectNo = _dbContext.Jobs.Where(x => x.LeadId == ),
                //                ContactedDate = l.CreationTime,
                //                SystemSize = j.SystemCapacity
                //            };

                //var query = from r in _dbContext.CpReferelDetails
                //            join l in _dbContext.Leads on r.Email equals l.Email into leftLead
                //            from ll in leftLead.First()
                //            where r.JobId == jobId
                //            select new RefereFriendDto
                //            {
                //                Name = r.FirstName + " " + r.LastName,
                //                RefferalDate = r.CreatedDate,
                //                ContactedDate = ll.CreationTime
                //            };

                //refereFriendDto = query.ToList();
            }
            catch
            {
            }

            await Task.CompletedTask;
            return refereFriendDto;

        }
    }
}
