﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(FreebieTransportId), Name = "IX_JobPromotions_FreebieTransportId")]
    [Index(nameof(JobId), Name = "IX_JobPromotions_JobId")]
    [Index(nameof(PromotionMasterId), Name = "IX_JobPromotions_PromotionMasterId")]
    [Index(nameof(TenantId), Name = "IX_JobPromotions_TenantId")]
    public partial class JobPromotion
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int JobId { get; set; }
        public int? PromotionMasterId { get; set; }
        public string Description { get; set; }
        public string TrackingNumber { get; set; }
        public DateTime? DispatchedDate { get; set; }
        public int? FreebieTransportId { get; set; }
        public bool? SmsSend { get; set; }
        public bool? EmailSend { get; set; }
        public DateTime? EmailSendDate { get; set; }
        public DateTime? SmsSendDate { get; set; }

        [ForeignKey(nameof(FreebieTransportId))]
        [InverseProperty("JobPromotions")]
        public virtual FreebieTransport FreebieTransport { get; set; }
        [ForeignKey(nameof(JobId))]
        [InverseProperty("JobPromotions")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(PromotionMasterId))]
        [InverseProperty("JobPromotions")]
        public virtual PromotionMaster PromotionMaster { get; set; }
    }
}
