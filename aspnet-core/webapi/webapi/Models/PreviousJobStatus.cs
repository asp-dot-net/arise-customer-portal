﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_PreviousJobStatuses_JobId")]
    [Index(nameof(LeadId), Name = "IX_PreviousJobStatuses_LeadId")]
    public partial class PreviousJobStatus
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int PreviousId { get; set; }
        [Column("CurrentID")]
        public int CurrentId { get; set; }
        public int? LeadId { get; set; }
        public int? JobId { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("PreviousJobStatuses")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(LeadId))]
        [InverseProperty("PreviousJobStatuses")]
        public virtual Lead Lead { get; set; }
    }
}
