﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(NotificationName), nameof(EntityTypeName), nameof(EntityId), nameof(UserId), Name = "IX_AbpNotificationSubscriptions_NotificationName_EntityTypeName_EntityId_UserId")]
    [Index(nameof(TenantId), nameof(NotificationName), nameof(EntityTypeName), nameof(EntityId), nameof(UserId), Name = "IX_AbpNotificationSubscriptions_TenantId_NotificationName_EntityTypeName_EntityId_UserId")]
    public partial class AbpNotificationSubscription
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        [StringLength(96)]
        public string EntityId { get; set; }
        [StringLength(512)]
        public string EntityTypeAssemblyQualifiedName { get; set; }
        [StringLength(250)]
        public string EntityTypeName { get; set; }
        [StringLength(96)]
        public string NotificationName { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
    }
}
