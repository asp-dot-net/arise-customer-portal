﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(ParentId), Name = "IX_AbpOrganizationUnits_ParentId")]
    [Index(nameof(TenantId), nameof(Code), Name = "IX_AbpOrganizationUnits_TenantId_Code")]
    public partial class AbpOrganizationUnit
    {
        public AbpOrganizationUnit()
        {
            InverseParent = new HashSet<AbpOrganizationUnit>();
        }

        [Key]
        public long Id { get; set; }
        [Required]
        [StringLength(95)]
        public string Code { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(128)]
        public string DisplayName { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public long? ParentId { get; set; }
        public int? TenantId { get; set; }
        [Required]
        public string Discriminator { get; set; }
        [Column("ABN")]
        public string Abn { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Logo { get; set; }
        public string OrganizationCode { get; set; }
        public string ProjectId { get; set; }
        public string GreenBoatPassword { get; set; }
        public string GreenBoatUsername { get; set; }
        [Column("defaultFromAddress")]
        public string DefaultFromAddress { get; set; }
        [Column("defaultFromDisplayName")]
        public string DefaultFromDisplayName { get; set; }
        public string FoneDynamicsAccountSid { get; set; }
        public string FoneDynamicsPhoneNumber { get; set; }
        public string FoneDynamicsPropertySid { get; set; }
        public string FoneDynamicsToken { get; set; }
        public string Email { get; set; }
        public string LogoFileName { get; set; }
        public string LogoFilePath { get; set; }
        public string Mobile { get; set; }
        [Column("cardNumber")]
        public string CardNumber { get; set; }
        [Column("cardholderName")]
        public string CardholderName { get; set; }
        [Column("cvn")]
        public string Cvn { get; set; }
        [Column("expiryDateMonth")]
        public string ExpiryDateMonth { get; set; }
        [Column("expiryDateYear")]
        public string ExpiryDateYear { get; set; }
        [Column("paymentMethod")]
        public string PaymentMethod { get; set; }
        public string MerchantId { get; set; }
        public string PublishKey { get; set; }
        public string SecrateKey { get; set; }
        [Column("businesscode")]
        public string Businesscode { get; set; }
        public string AuthorizationKey { get; set; }
        public string WestPacPublishKey { get; set; }
        public string WestPacSecreteKey { get; set; }
        public string SurchargeAuthorizationKey { get; set; }
        public string SurchargePublishKey { get; set; }
        public string SurchargeSecrateKey { get; set; }

        [ForeignKey(nameof(ParentId))]
        [InverseProperty(nameof(AbpOrganizationUnit.InverseParent))]
        public virtual AbpOrganizationUnit Parent { get; set; }
        [InverseProperty(nameof(AbpOrganizationUnit.Parent))]
        public virtual ICollection<AbpOrganizationUnit> InverseParent { get; set; }
    }
}
