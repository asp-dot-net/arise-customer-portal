﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_RefundReasons_TenantId")]
    public partial class RefundReason
    {
        public RefundReason()
        {
            JobRefunds = new HashSet<JobRefund>();
        }

        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        public bool? IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }

        [InverseProperty(nameof(JobRefund.RefundReason))]
        public virtual ICollection<JobRefund> JobRefunds { get; set; }
    }
}
