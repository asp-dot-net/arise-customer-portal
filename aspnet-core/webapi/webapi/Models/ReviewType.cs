﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class ReviewType
    {
        public ReviewType()
        {
            JobReviews = new HashSet<JobReview>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        [Required]
        public string Name { get; set; }
        public string ReviewLink { get; set; }

        [InverseProperty(nameof(JobReview.ReviewType))]
        public virtual ICollection<JobReview> JobReviews { get; set; }
    }
}
