﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class PromotionResponseStatus
    {
        public PromotionResponseStatus()
        {
            PromotionUsers = new HashSet<PromotionUser>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [InverseProperty(nameof(PromotionUser.PromotionResponseStatus))]
        public virtual ICollection<PromotionUser> PromotionUsers { get; set; }
    }
}
