﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(ProductTypeId), Name = "IX_ProductItems_ProductTypeId")]
    [Index(nameof(TenantId), Name = "IX_ProductItems_TenantId")]
    public partial class ProductItem
    {
        public ProductItem()
        {
            JobProductItems = new HashSet<JobProductItem>();
            ProductItemLocations = new HashSet<ProductItemLocation>();
        }

        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        [Required]
        [StringLength(500)]
        public string Name { get; set; }
        [StringLength(200)]
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        [StringLength(50)]
        public string Series { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Size { get; set; }
        public int? Code { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int ProductTypeId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        public bool? IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        [Required]
        public bool? Active { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }

        [ForeignKey(nameof(ProductTypeId))]
        [InverseProperty("ProductItems")]
        public virtual ProductType ProductType { get; set; }
        [InverseProperty(nameof(JobProductItem.ProductItem))]
        public virtual ICollection<JobProductItem> JobProductItems { get; set; }
        [InverseProperty(nameof(ProductItemLocation.ProductItem))]
        public virtual ICollection<ProductItemLocation> ProductItemLocations { get; set; }
    }
}
