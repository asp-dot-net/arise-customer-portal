﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("LeadStatus")]
    public partial class LeadStatus
    {
        public LeadStatus()
        {
            Leads = new HashSet<Lead>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        public string Status { get; set; }

        [InverseProperty(nameof(Lead.LeadStatus))]
        public virtual ICollection<Lead> Leads { get; set; }
    }
}
