﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(InvoicePaymentMethodId), Name = "IX_InvoiceImportDatas_InvoicePaymentMethodId")]
    [Index(nameof(JobId), Name = "IX_InvoiceImportDatas_JobId")]
    public partial class InvoiceImportData
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public DateTime? Date { get; set; }
        public string JobNumber { get; set; }
        public int? JobId { get; set; }
        public string InvoicePaymentmothodName { get; set; }
        public int? InvoicePaymentMethodId { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? PaidAmmount { get; set; }
        public string AllotedBy { get; set; }
        public string InvoiceNotesDescription { get; set; }
        public string PurchaseNumber { get; set; }
        public string ReceiptNumber { get; set; }
        [Column("SSCharge", TypeName = "decimal(18, 2)")]
        public decimal? Sscharge { get; set; }

        [ForeignKey(nameof(InvoicePaymentMethodId))]
        [InverseProperty("InvoiceImportData")]
        public virtual InvoicePaymentMethod InvoicePaymentMethod { get; set; }
        [ForeignKey(nameof(JobId))]
        [InverseProperty("InvoiceImportData")]
        public virtual Job Job { get; set; }
    }
}
