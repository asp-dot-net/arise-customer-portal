﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("LeadtrackerHistory")]
    public partial class LeadtrackerHistory
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public string FieldName { get; set; }
        public string PrevValue { get; set; }
        public string CurValue { get; set; }
        public string Action { get; set; }
        public DateTime LastmodifiedDateTime { get; set; }
        public int LeadActionId { get; set; }
        public int LeadId { get; set; }
    }
}
