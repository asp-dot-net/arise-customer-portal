﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("ProductItemLocation")]
    [Index(nameof(ProductItemId), Name = "IX_ProductItemLocation_ProductItemId")]
    [Index(nameof(WarehouselocationId), Name = "IX_ProductItemLocation_WarehouselocationId")]
    public partial class ProductItemLocation
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int ProductItemId { get; set; }
        public int WarehouselocationId { get; set; }
        [Required]
        public bool? SalesTag { get; set; }

        [ForeignKey(nameof(ProductItemId))]
        [InverseProperty("ProductItemLocations")]
        public virtual ProductItem ProductItem { get; set; }
        [ForeignKey(nameof(WarehouselocationId))]
        [InverseProperty(nameof(WareHouseLocation.ProductItemLocations))]
        public virtual WareHouseLocation Warehouselocation { get; set; }
    }
}
