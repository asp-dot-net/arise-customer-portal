﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(LeadStatusId), Name = "IX_Leads_LeadStatusId")]
    [Index(nameof(TenantId), Name = "IX_Leads_TenantId")]
    public partial class Lead
    {
        public Lead()
        {
            Jobs = new HashSet<Job>();
            LeadActivityLogs = new HashSet<LeadActivityLog>();
            PreviousJobStatuses = new HashSet<PreviousJobStatus>();
            PromotionUsers = new HashSet<PromotionUser>();
            Services = new HashSet<Service>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Requirements { get; set; }
        [Column("AssignToUserID")]
        public int? AssignToUserId { get; set; }
        public int LeadStatusId { get; set; }
        public string PostCode { get; set; }
        public string StreetName { get; set; }
        public string StreetNo { get; set; }
        public string StreetType { get; set; }
        public string UnitNo { get; set; }
        public string UnitType { get; set; }
        [Column("latitude")]
        public string Latitude { get; set; }
        [Column("longitude")]
        public string Longitude { get; set; }
        public bool? IsDuplicate { get; set; }
        [Required]
        public bool? IsPromotion { get; set; }
        [Column("ABN")]
        public string Abn { get; set; }
        public string AltPhone { get; set; }
        public string AngleType { get; set; }
        public string Area { get; set; }
        public string CompanyName { get; set; }
        public string Fax { get; set; }
        public string HouseAgeType { get; set; }
        public string LeadSource { get; set; }
        public string RoofType { get; set; }
        public string SolarType { get; set; }
        public string State { get; set; }
        public string StoryType { get; set; }
        public string Suburb { get; set; }
        public string SystemType { get; set; }
        public string Type { get; set; }
        public int? LeadSourceId { get; set; }
        public int? StateId { get; set; }
        public int? SuburbId { get; set; }
        public string RejectReason { get; set; }
        public string IsGoogle { get; set; }
        public int? CancelReasonId { get; set; }
        public int? RejectReasonId { get; set; }
        public int IsExternalLead { get; set; }
        public int OrganizationId { get; set; }
        public bool? IsWebDuplicate { get; set; }
        public string ExcelAddress { get; set; }
        public DateTime? ChangeStatusDate { get; set; }
        public DateTime? LeadAssignDate { get; set; }

        [ForeignKey(nameof(LeadStatusId))]
        [InverseProperty("Leads")]
        public virtual LeadStatus LeadStatus { get; set; }
        [InverseProperty(nameof(Job.Lead))]
        public virtual ICollection<Job> Jobs { get; set; }
        [InverseProperty(nameof(LeadActivityLog.Lead))]
        public virtual ICollection<LeadActivityLog> LeadActivityLogs { get; set; }
        [InverseProperty(nameof(PreviousJobStatus.Lead))]
        public virtual ICollection<PreviousJobStatus> PreviousJobStatuses { get; set; }
        [InverseProperty(nameof(PromotionUser.Lead))]
        public virtual ICollection<PromotionUser> PromotionUsers { get; set; }
        [InverseProperty(nameof(Service.Lead))]
        public virtual ICollection<Service> Services { get; set; }
    }
}
