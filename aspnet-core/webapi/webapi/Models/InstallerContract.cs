﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(UserId), Name = "IX_InstallerContracts_UserId")]
    public partial class InstallerContract
    {
        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public int OrganizationId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.InstallerContracts))]
        public virtual AbpUser User { get; set; }
    }
}
