﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(DocumentTypeId), Name = "IX_Documents_DocumentTypeId")]
    [Index(nameof(JobId), Name = "IX_Documents_JobId")]
    public partial class Document
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? DocumentTypeId { get; set; }
        public Guid? MediaId { get; set; }
        public int? JobId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string NearMapImage1 { get; set; }
        public string NearMapImage2 { get; set; }
        public string NearMapImage3 { get; set; }

        [ForeignKey(nameof(DocumentTypeId))]
        [InverseProperty("Documents")]
        public virtual DocumentType DocumentType { get; set; }
        [ForeignKey(nameof(JobId))]
        [InverseProperty("Documents")]
        public virtual Job Job { get; set; }
    }
}
