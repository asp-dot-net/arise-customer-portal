﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("WareHouseLocation")]
    public partial class WareHouseLocation
    {
        public WareHouseLocation()
        {
            ProductItemLocations = new HashSet<ProductItemLocation>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        [Column("location")]
        public string Location { get; set; }
        [Column("state")]
        public string State { get; set; }

        [InverseProperty(nameof(ProductItemLocation.Warehouselocation))]
        public virtual ICollection<ProductItemLocation> ProductItemLocations { get; set; }
    }
}
