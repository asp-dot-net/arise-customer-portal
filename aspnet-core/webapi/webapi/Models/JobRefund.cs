﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_JobRefunds_JobId")]
    [Index(nameof(PaymentOptionId), Name = "IX_JobRefunds_PaymentOptionId")]
    [Index(nameof(RefundReasonId), Name = "IX_JobRefunds_RefundReasonId")]
    [Index(nameof(TenantId), Name = "IX_JobRefunds_TenantId")]
    public partial class JobRefund
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int? Amount { get; set; }
        [Required]
        public string BankName { get; set; }
        public string AccountName { get; set; }
        [Column("BSBNo")]
        public string Bsbno { get; set; }
        [Required]
        public string AccountNo { get; set; }
        public string Notes { get; set; }
        public DateTime? PaidDate { get; set; }
        public string Remarks { get; set; }
        public int? PaymentOptionId { get; set; }
        public int? JobId { get; set; }
        public int? RefundReasonId { get; set; }
        public string ReceiptNo { get; set; }
        public bool? JobRefundEmailSend { get; set; }
        public DateTime? JobRefundEmailSendDate { get; set; }
        public bool? JobRefundSmsSend { get; set; }
        public DateTime? JobRefundSmsSendDate { get; set; }
        public bool? IsVerify { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("JobRefunds")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(PaymentOptionId))]
        [InverseProperty("JobRefunds")]
        public virtual PaymentOption PaymentOption { get; set; }
        [ForeignKey(nameof(RefundReasonId))]
        [InverseProperty("JobRefunds")]
        public virtual RefundReason RefundReason { get; set; }
    }
}
