﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(IsAbandoned), nameof(NextTryTime), Name = "IX_AbpBackgroundJobs_IsAbandoned_NextTryTime")]
    public partial class AbpBackgroundJob
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public bool IsAbandoned { get; set; }
        [Required]
        public string JobArgs { get; set; }
        [Required]
        [StringLength(512)]
        public string JobType { get; set; }
        public DateTime? LastTryTime { get; set; }
        public DateTime NextTryTime { get; set; }
        public byte Priority { get; set; }
        public short TryCount { get; set; }
    }
}
