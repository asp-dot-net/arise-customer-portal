﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_Quotations_JobId")]
    public partial class Quotation
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public DateTime QuoteDate { get; set; }
        public bool IsSigned { get; set; }
        public Guid? DocumentId { get; set; }
        public int? JobId { get; set; }
        public DateTime? QuoteAcceptDate { get; set; }
        public string CustSignFileName { get; set; }
        public string CustSignLatitude { get; set; }
        public string CustSignLongitude { get; set; }
        public string QuoteFileName { get; set; }
        public int TenantId { get; set; }
        public string QuoteFilePath { get; set; }
        public string SignedQuoteFileName { get; set; }
        public string QuoteNumber { get; set; }
        [Column("CustSignIP")]
        public string CustSignIp { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("Quotations")]
        public virtual Job Job { get; set; }
    }
}
