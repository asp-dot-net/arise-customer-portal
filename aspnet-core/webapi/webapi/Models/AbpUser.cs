﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(CreatorUserId), Name = "IX_AbpUsers_CreatorUserId")]
    [Index(nameof(DeleterUserId), Name = "IX_AbpUsers_DeleterUserId")]
    [Index(nameof(LastModifierUserId), Name = "IX_AbpUsers_LastModifierUserId")]
    [Index(nameof(TenantId), nameof(NormalizedEmailAddress), Name = "IX_AbpUsers_TenantId_NormalizedEmailAddress")]
    [Index(nameof(TenantId), nameof(NormalizedUserName), Name = "IX_AbpUsers_TenantId_NormalizedUserName")]
    public partial class AbpUser
    {
        public AbpUser()
        {
            AbpPermissions = new HashSet<AbpPermission>();
            AbpRoleCreatorUsers = new HashSet<AbpRole>();
            AbpRoleDeleterUsers = new HashSet<AbpRole>();
            AbpRoleLastModifierUsers = new HashSet<AbpRole>();
            AbpSettings = new HashSet<AbpSetting>();
            AbpTenantCreatorUsers = new HashSet<AbpTenant>();
            AbpTenantDeleterUsers = new HashSet<AbpTenant>();
            AbpTenantLastModifierUsers = new HashSet<AbpTenant>();
            AbpUserClaims = new HashSet<AbpUserClaim>();
            AbpUserLogins = new HashSet<AbpUserLogin>();
            AbpUserOrganizationUnits = new HashSet<AbpUserOrganizationUnit>();
            AbpUserRoles = new HashSet<AbpUserRole>();
            AbpUserTokens = new HashSet<AbpUserToken>();
            InstallerAddresses = new HashSet<InstallerAddress>();
            InstallerContracts = new HashSet<InstallerContract>();
            InstallerDetails = new HashSet<InstallerDetail>();
            InverseCreatorUser = new HashSet<AbpUser>();
            InverseDeleterUser = new HashSet<AbpUser>();
            InverseLastModifierUser = new HashSet<AbpUser>();
            InvoicePaymentRefundByNavigations = new HashSet<InvoicePayment>();
            InvoicePaymentUsers = new HashSet<InvoicePayment>();
            InvoicePaymentVerifiedByNavigations = new HashSet<InvoicePayment>();
            UserTeams = new HashSet<UserTeam>();
        }

        [Key]
        public long Id { get; set; }
        public int AccessFailedCount { get; set; }
        [StringLength(64)]
        public string AuthenticationSource { get; set; }
        [StringLength(128)]
        public string ConcurrencyStamp { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(256)]
        public string EmailAddress { get; set; }
        [StringLength(328)]
        public string EmailConfirmationCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public bool IsLockoutEnabled { get; set; }
        public bool IsPhoneNumberConfirmed { get; set; }
        public bool IsTwoFactorEnabled { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        [Required]
        [StringLength(64)]
        public string Name { get; set; }
        [Required]
        [StringLength(256)]
        public string NormalizedEmailAddress { get; set; }
        [Required]
        [StringLength(256)]
        public string NormalizedUserName { get; set; }
        [Required]
        [StringLength(128)]
        public string Password { get; set; }
        [StringLength(328)]
        public string PasswordResetCode { get; set; }
        [StringLength(32)]
        public string PhoneNumber { get; set; }
        public Guid? ProfilePictureId { get; set; }
        [StringLength(128)]
        public string SecurityStamp { get; set; }
        public bool ShouldChangePasswordOnNextLogin { get; set; }
        [Required]
        [StringLength(64)]
        public string Surname { get; set; }
        public int? TenantId { get; set; }
        [Required]
        [StringLength(256)]
        public string UserName { get; set; }
        public string SignInToken { get; set; }
        public DateTime? SignInTokenExpireTimeUtc { get; set; }
        public string GoogleAuthenticatorKey { get; set; }
        public int CategoryId { get; set; }
        public string Target { get; set; }
        [Column("MonthlyEMailQuote")]
        public int MonthlyEmailQuote { get; set; }
        [Column("MonthlySMSQuote")]
        public int MonthlySmsquote { get; set; }
        public string CompanyName { get; set; }
        public string Mobile { get; set; }
        [Required]
        [Column("ismyinstalleruser")]
        public bool? Ismyinstalleruser { get; set; }
        public string AreaName { get; set; }
        public int SourceTypeId { get; set; }

        [ForeignKey(nameof(CreatorUserId))]
        [InverseProperty(nameof(AbpUser.InverseCreatorUser))]
        public virtual AbpUser CreatorUser { get; set; }
        [ForeignKey(nameof(DeleterUserId))]
        [InverseProperty(nameof(AbpUser.InverseDeleterUser))]
        public virtual AbpUser DeleterUser { get; set; }
        [ForeignKey(nameof(LastModifierUserId))]
        [InverseProperty(nameof(AbpUser.InverseLastModifierUser))]
        public virtual AbpUser LastModifierUser { get; set; }
        [InverseProperty(nameof(AbpPermission.User))]
        public virtual ICollection<AbpPermission> AbpPermissions { get; set; }
        [InverseProperty(nameof(AbpRole.CreatorUser))]
        public virtual ICollection<AbpRole> AbpRoleCreatorUsers { get; set; }
        [InverseProperty(nameof(AbpRole.DeleterUser))]
        public virtual ICollection<AbpRole> AbpRoleDeleterUsers { get; set; }
        [InverseProperty(nameof(AbpRole.LastModifierUser))]
        public virtual ICollection<AbpRole> AbpRoleLastModifierUsers { get; set; }
        [InverseProperty(nameof(AbpSetting.User))]
        public virtual ICollection<AbpSetting> AbpSettings { get; set; }
        [InverseProperty(nameof(AbpTenant.CreatorUser))]
        public virtual ICollection<AbpTenant> AbpTenantCreatorUsers { get; set; }
        [InverseProperty(nameof(AbpTenant.DeleterUser))]
        public virtual ICollection<AbpTenant> AbpTenantDeleterUsers { get; set; }
        [InverseProperty(nameof(AbpTenant.LastModifierUser))]
        public virtual ICollection<AbpTenant> AbpTenantLastModifierUsers { get; set; }
        [InverseProperty(nameof(AbpUserClaim.User))]
        public virtual ICollection<AbpUserClaim> AbpUserClaims { get; set; }
        [InverseProperty(nameof(AbpUserLogin.User))]
        public virtual ICollection<AbpUserLogin> AbpUserLogins { get; set; }
        [InverseProperty(nameof(AbpUserOrganizationUnit.User))]
        public virtual ICollection<AbpUserOrganizationUnit> AbpUserOrganizationUnits { get; set; }
        [InverseProperty(nameof(AbpUserRole.User))]
        public virtual ICollection<AbpUserRole> AbpUserRoles { get; set; }
        [InverseProperty(nameof(AbpUserToken.User))]
        public virtual ICollection<AbpUserToken> AbpUserTokens { get; set; }
        [InverseProperty(nameof(InstallerAddress.User))]
        public virtual ICollection<InstallerAddress> InstallerAddresses { get; set; }
        [InverseProperty(nameof(InstallerContract.User))]
        public virtual ICollection<InstallerContract> InstallerContracts { get; set; }
        [InverseProperty(nameof(InstallerDetail.User))]
        public virtual ICollection<InstallerDetail> InstallerDetails { get; set; }
        [InverseProperty(nameof(AbpUser.CreatorUser))]
        public virtual ICollection<AbpUser> InverseCreatorUser { get; set; }
        [InverseProperty(nameof(AbpUser.DeleterUser))]
        public virtual ICollection<AbpUser> InverseDeleterUser { get; set; }
        [InverseProperty(nameof(AbpUser.LastModifierUser))]
        public virtual ICollection<AbpUser> InverseLastModifierUser { get; set; }
        [InverseProperty(nameof(InvoicePayment.RefundByNavigation))]
        public virtual ICollection<InvoicePayment> InvoicePaymentRefundByNavigations { get; set; }
        [InverseProperty(nameof(InvoicePayment.User))]
        public virtual ICollection<InvoicePayment> InvoicePaymentUsers { get; set; }
        [InverseProperty(nameof(InvoicePayment.VerifiedByNavigation))]
        public virtual ICollection<InvoicePayment> InvoicePaymentVerifiedByNavigations { get; set; }
        [InverseProperty(nameof(UserTeam.User))]
        public virtual ICollection<UserTeam> UserTeams { get; set; }
    }
}
