﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_InstallerInvoices_JobId")]
    public partial class InstallerInvoice
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int? JobId { get; set; }
        public int? InvTypeId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Amount { get; set; }
        public DateTime? InvDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? AdvanceAmount { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? LessDeductAmount { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? TotalAmount { get; set; }
        public DateTime? AdvancePayDate { get; set; }
        public DateTime? PayDate { get; set; }
        public DateTime? PaymentTypeId { get; set; }
        public int? InstallerId { get; set; }
        public string Notes { get; set; }
        public DateTime? Date { get; set; }
        public string Remarks { get; set; }
        public bool IsPaid { get; set; }
        public bool IsVerify { get; set; }
        public int? PaymentsTypeId { get; set; }
        public int? InvNo { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        [Column("ActualPanels_Installed")]
        public int? ActualPanelsInstalled { get; set; }
        [Column("AllGood_NotGood")]
        public string AllGoodNotGood { get; set; }
        [Column("CES")]
        public int? Ces { get; set; }
        [Column("Cx_Sign")]
        public int? CxSign { get; set; }
        public string EmailSent { get; set; }
        [Column("Front_of_property")]
        public int? FrontOfProperty { get; set; }
        [Column("Inst_Des_Ele_sign")]
        public int? InstDesEleSign { get; set; }
        public int? InstalaltionPic { get; set; }
        [Column("Installation_Maintenance_Inspection")]
        public string InstallationMaintenanceInspection { get; set; }
        [Column("Installer_selfie")]
        public int? InstallerSelfie { get; set; }
        public int? InverterSerialNo { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? InvoiceAmount { get; set; }
        public string InvoiceNo { get; set; }
        [Column("Noof_Panels_Portal")]
        public int? NoofPanelsPortal { get; set; }
        [Column("Noof_Panels_invoice")]
        public int? NoofPanelsInvoice { get; set; }
        public string NotesOrReasonforPending { get; set; }
        public int? PanelsSerialNo { get; set; }
        [Column("Remark_if_owing")]
        public string RemarkIfOwing { get; set; }
        public int? Splits { get; set; }
        public int? Traded { get; set; }
        [Column("TravelKMfromWarehouse")]
        public string TravelKmfromWarehouse { get; set; }
        [Column("Wi_FiDongle")]
        public int? WiFiDongle { get; set; }
        [Column("otherExtraInvoiceNumber")]
        public string OtherExtraInvoiceNumber { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ApproedAmount { get; set; }
        public DateTime? ApproedDate { get; set; }
        public string ApprovedNotes { get; set; }
        public string BankRefNo { get; set; }
        [Column("invoiceIssuedDate")]
        public DateTime? InvoiceIssuedDate { get; set; }
        [Column("emaisenddate")]
        public DateTime? Emaisenddate { get; set; }
        public bool? SmsSend { get; set; }
        public DateTime? SmsSendDate { get; set; }
        [Column("installerEmailSend")]
        public bool? InstallerEmailSend { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("InstallerInvoices")]
        public virtual Job Job { get; set; }
    }
}
