﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TargetTenantId), nameof(TargetUserId), nameof(ReadState), Name = "IX_AppChatMessages_TargetTenantId_TargetUserId_ReadState")]
    [Index(nameof(TargetTenantId), nameof(UserId), nameof(ReadState), Name = "IX_AppChatMessages_TargetTenantId_UserId_ReadState")]
    [Index(nameof(TenantId), nameof(TargetUserId), nameof(ReadState), Name = "IX_AppChatMessages_TenantId_TargetUserId_ReadState")]
    [Index(nameof(TenantId), nameof(UserId), nameof(ReadState), Name = "IX_AppChatMessages_TenantId_UserId_ReadState")]
    public partial class AppChatMessage
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        [Required]
        public string Message { get; set; }
        public int ReadState { get; set; }
        public int Side { get; set; }
        public int? TargetTenantId { get; set; }
        public long TargetUserId { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public Guid? SharedMessageId { get; set; }
        public int ReceiverReadState { get; set; }
    }
}
