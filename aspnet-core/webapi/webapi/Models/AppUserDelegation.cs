﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(SourceUserId), Name = "IX_AppUserDelegations_TenantId_SourceUserId")]
    [Index(nameof(TenantId), nameof(TargetUserId), Name = "IX_AppUserDelegations_TenantId_TargetUserId")]
    public partial class AppUserDelegation
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public long SourceUserId { get; set; }
        public long TargetUserId { get; set; }
        public int? TenantId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
