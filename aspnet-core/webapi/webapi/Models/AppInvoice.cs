﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class AppInvoice
    {
        [Key]
        public int Id { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string TenantAddress { get; set; }
        public string TenantLegalName { get; set; }
        public string TenantTaxNo { get; set; }
    }
}
