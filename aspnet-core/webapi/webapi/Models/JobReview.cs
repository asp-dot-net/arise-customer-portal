﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("JobReview")]
    [Index(nameof(JobId), Name = "IX_JobReview_JobId")]
    [Index(nameof(ReviewTypeId), Name = "IX_JobReview_ReviewTypeId")]
    public partial class JobReview
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int? Rating { get; set; }
        public string ReviewNotes { get; set; }
        public int? ReviewTypeId { get; set; }
        public int? JobId { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("JobReviews")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(ReviewTypeId))]
        [InverseProperty("JobReviews")]
        public virtual ReviewType ReviewType { get; set; }
    }
}
