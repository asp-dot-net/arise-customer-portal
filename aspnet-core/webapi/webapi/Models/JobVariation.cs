﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_JobVariations_JobId")]
    [Index(nameof(TenantId), Name = "IX_JobVariations_TenantId")]
    [Index(nameof(VariationId), Name = "IX_JobVariations_VariationId")]
    public partial class JobVariation
    {
        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Cost { get; set; }
        public int? VariationId { get; set; }
        public int? JobId { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("JobVariations")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(VariationId))]
        [InverseProperty("JobVariations")]
        public virtual Variation Variation { get; set; }
    }
}
