﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("Temp")]
    public partial class Temp
    {
        [Key]
        public int Id { get; set; }
        [Column("colPhone")]
        public string ColPhone { get; set; }
        [Column("colText")]
        public string ColText { get; set; }
        public int? LeadId { get; set; }
    }
}
