﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class AbpWebhookSubscription
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int? TenantId { get; set; }
        [Required]
        public string WebhookUri { get; set; }
        [Required]
        public string Secret { get; set; }
        public bool IsActive { get; set; }
        public string Webhooks { get; set; }
        public string Headers { get; set; }
    }
}
