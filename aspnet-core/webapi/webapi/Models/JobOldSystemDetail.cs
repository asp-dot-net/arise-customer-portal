﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_JobOldSystemDetails_JobId")]
    public partial class JobOldSystemDetail
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int? JobId { get; set; }
        public string Name { get; set; }
        public int? Quantity { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("JobOldSystemDetails")]
        public virtual Job Job { get; set; }
    }
}
