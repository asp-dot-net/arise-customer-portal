﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(SubjectId), nameof(ClientId), nameof(Type), Name = "IX_AbpPersistedGrants_SubjectId_ClientId_Type")]
    public partial class AbpPersistedGrant
    {
        [Key]
        [StringLength(200)]
        public string Id { get; set; }
        [Required]
        [StringLength(200)]
        public string ClientId { get; set; }
        public DateTime CreationTime { get; set; }
        [Required]
        public string Data { get; set; }
        public DateTime? Expiration { get; set; }
        [StringLength(200)]
        public string SubjectId { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
    }
}
