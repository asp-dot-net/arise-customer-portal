﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class GreenBoatData
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int? CompanyId { get; set; }
        public int ProjectNo { get; set; }
        public string VendorJobId { get; set; }
        [Column("JobID")]
        public int? JobId { get; set; }
        public string Title { get; set; }
        public string RefNumber { get; set; }
        public string Description { get; set; }
        public int? JobType { get; set; }
        public int? JobStage { get; set; }
        public int? Priority { get; set; }
        public string InstallationDate { get; set; }
        public string CreatedDate { get; set; }
        public bool IsGst { get; set; }
        [Column("OwnerType_JOD")]
        public string OwnerTypeJod { get; set; }
        [Column("CompanyName_JOD")]
        public string CompanyNameJod { get; set; }
        [Column("FirstName_JOD")]
        public string FirstNameJod { get; set; }
        [Column("LastName_JOD")]
        public string LastNameJod { get; set; }
        [Column("Phone_JOD")]
        public string PhoneJod { get; set; }
        [Column("Mobile_JOD")]
        public string MobileJod { get; set; }
        [Column("Email_JOD")]
        public string EmailJod { get; set; }
        [Column("IsPostalAddress_JOD")]
        public bool IsPostalAddressJod { get; set; }
        [Column("UnitTypeID_JOD")]
        public string UnitTypeIdJod { get; set; }
        [Column("UnitNumber_JOD")]
        public string UnitNumberJod { get; set; }
        [Column("StreetNumber_JOD")]
        public string StreetNumberJod { get; set; }
        [Column("StreetName_JOD")]
        public string StreetNameJod { get; set; }
        [Column("StreetTypeID_JOD")]
        public int? StreetTypeIdJod { get; set; }
        [Column("Town_JOD")]
        public string TownJod { get; set; }
        [Column("State_JOD")]
        public string StateJod { get; set; }
        [Column("PostCode_JOD")]
        public string PostCodeJod { get; set; }
        [Column("DistributorID_JID")]
        public string DistributorIdJid { get; set; }
        [Column("MeterNumber_JID")]
        public string MeterNumberJid { get; set; }
        [Column("PhaseProperty_JID")]
        public string PhasePropertyJid { get; set; }
        [Column("ElectricityProviderID_JID")]
        public string ElectricityProviderIdJid { get; set; }
        [Column("IsSameAsOwnerAddress_JID")]
        public bool IsSameAsOwnerAddressJid { get; set; }
        [Column("IsPostalAddress_JID")]
        public bool IsPostalAddressJid { get; set; }
        [Column("UnitTypeID_JID")]
        public string UnitTypeIdJid { get; set; }
        [Column("UnitNumber_JID")]
        public string UnitNumberJid { get; set; }
        [Column("StreetNumber_JID")]
        public string StreetNumberJid { get; set; }
        [Column("StreetName_JID")]
        public string StreetNameJid { get; set; }
        [Column("StreetTypeID_JID")]
        public int? StreetTypeIdJid { get; set; }
        [Column("Town_JID")]
        public string TownJid { get; set; }
        [Column("State_JID")]
        public string StateJid { get; set; }
        [Column("PostCode_JID")]
        public string PostCodeJid { get; set; }
        [Column("NMI_JID")]
        public string NmiJid { get; set; }
        [Column("SystemSize_JID")]
        public string SystemSizeJid { get; set; }
        [Column("PropertyType_JID")]
        public string PropertyTypeJid { get; set; }
        [Column("SingleMultipleStory_JID")]
        public string SingleMultipleStoryJid { get; set; }
        [Column("InstallingNewPanel_JID")]
        public string InstallingNewPanelJid { get; set; }
        [Column("Location_JID")]
        public string LocationJid { get; set; }
        [Column("ExistingSystem_JID")]
        public bool ExistingSystemJid { get; set; }
        [Column("ExistingSystemSize_JID")]
        public string ExistingSystemSizeJid { get; set; }
        [Column("SystemLocation_JID")]
        public string SystemLocationJid { get; set; }
        [Column("NoOfPanels_JID")]
        public string NoOfPanelsJid { get; set; }
        [Column("AdditionalInstallationInformation_JID")]
        public string AdditionalInstallationInformationJid { get; set; }
        [Column("SystemSize_JSD")]
        public string SystemSizeJsd { get; set; }
        [Column("SerialNumbers_JSD")]
        public string SerialNumbersJsd { get; set; }
        [Column("NoOfPanel_JSD")]
        public string NoOfPanelJsd { get; set; }
        [Column("AdditionalCapacityNotes_JSTC")]
        public string AdditionalCapacityNotesJstc { get; set; }
        [Column("TypeOfConnection_JSTC")]
        public string TypeOfConnectionJstc { get; set; }
        [Column("SystemMountingType_JSTC")]
        public string SystemMountingTypeJstc { get; set; }
        [Column("DeemingPeriod_JSTC")]
        public string DeemingPeriodJstc { get; set; }
        [Column("CertificateCreated_JSTC")]
        public string CertificateCreatedJstc { get; set; }
        [Column("FailedAccreditationCode_JSTC")]
        public string FailedAccreditationCodeJstc { get; set; }
        [Column("FailedReason_JSTC")]
        public string FailedReasonJstc { get; set; }
        [Column("MultipleSGUAddress_JSTC")]
        public string MultipleSguaddressJstc { get; set; }
        [Column("Location_JSTC")]
        public string LocationJstc { get; set; }
        [Column("AdditionalLocationInformation_JSTC")]
        public string AdditionalLocationInformationJstc { get; set; }
        [Column("AdditionalSystemInformation_JSTC")]
        public string AdditionalSystemInformationJstc { get; set; }
        [Column("FirstName_IV")]
        public string FirstNameIv { get; set; }
        [Column("LastName_IV")]
        public string LastNameIv { get; set; }
        [Column("Email_IV")]
        public string EmailIv { get; set; }
        [Column("Phone_IV")]
        public string PhoneIv { get; set; }
        [Column("Mobile_IV")]
        public string MobileIv { get; set; }
        [Column("CECAccreditationNumber_IV")]
        public string CecaccreditationNumberIv { get; set; }
        [Column("ElectricalContractorsLicenseNumber_IV")]
        public string ElectricalContractorsLicenseNumberIv { get; set; }
        [Column("IsPostalAddress_IV")]
        public bool IsPostalAddressIv { get; set; }
        [Column("UnitTypeID_IV")]
        public int? UnitTypeIdIv { get; set; }
        [Column("UnitNumber_IV")]
        public string UnitNumberIv { get; set; }
        [Column("StreetNumber_IV")]
        public string StreetNumberIv { get; set; }
        [Column("StreetName_IV")]
        public string StreetNameIv { get; set; }
        [Column("StreetTypeID_IV")]
        public int? StreetTypeIdIv { get; set; }
        [Column("Town_IV")]
        public string TownIv { get; set; }
        [Column("State_IV")]
        public string StateIv { get; set; }
        [Column("PostCode_IV")]
        public string PostCodeIv { get; set; }
        [Column("FirstName_DV")]
        public string FirstNameDv { get; set; }
        [Column("LastName_DV")]
        public string LastNameDv { get; set; }
        [Column("Email_DV")]
        public string EmailDv { get; set; }
        [Column("Phone_DV")]
        public string PhoneDv { get; set; }
        [Column("Mobile_DV")]
        public string MobileDv { get; set; }
        [Column("CECAccreditationNumber_DV")]
        public string CecaccreditationNumberDv { get; set; }
        [Column("ElectricalContractorsLicenseNumber_DV")]
        public string ElectricalContractorsLicenseNumberDv { get; set; }
        [Column("IsPostalAddress_DV")]
        public bool IsPostalAddressDv { get; set; }
        [Column("UnitTypeID_DV")]
        public int? UnitTypeIdDv { get; set; }
        [Column("UnitNumber_DV")]
        public string UnitNumberDv { get; set; }
        [Column("StreetNumber_DV")]
        public string StreetNumberDv { get; set; }
        [Column("StreetName_DV")]
        public string StreetNameDv { get; set; }
        [Column("StreetTypeID_DV")]
        public int? StreetTypeIdDv { get; set; }
        [Column("Town_DV")]
        public string TownDv { get; set; }
        [Column("State_DV")]
        public string StateDv { get; set; }
        [Column("PostCode_DV")]
        public string PostCodeDv { get; set; }
        [Column("CompanyName_JE")]
        public string CompanyNameJe { get; set; }
        [Column("FirstName_JE")]
        public string FirstNameJe { get; set; }
        [Column("LastName_JE")]
        public string LastNameJe { get; set; }
        [Column("Email_JE")]
        public string EmailJe { get; set; }
        [Column("Phone_JE")]
        public string PhoneJe { get; set; }
        [Column("Mobile_JE")]
        public string MobileJe { get; set; }
        [Column("LicenseNumber_JE")]
        public string LicenseNumberJe { get; set; }
        [Column("IsPostalAddress_JE")]
        public bool IsPostalAddressJe { get; set; }
        [Column("UnitTypeID_JE")]
        public string UnitTypeIdJe { get; set; }
        [Column("UnitNumber_JE")]
        public string UnitNumberJe { get; set; }
        [Column("StreetNumber_JE")]
        public string StreetNumberJe { get; set; }
        [Column("StreetName_JE")]
        public string StreetNameJe { get; set; }
        [Column("StreetTypeID_JE")]
        public string StreetTypeIdJe { get; set; }
        [Column("Town_JE")]
        public string TownJe { get; set; }
        [Column("State_JE")]
        public string StateJe { get; set; }
        [Column("PostCode_JE")]
        public string PostCodeJe { get; set; }
        [Column("Brand_LstJID")]
        public string BrandLstJid { get; set; }
        [Column("Model_LstJID")]
        public string ModelLstJid { get; set; }
        [Column("Series_LstJID")]
        public string SeriesLstJid { get; set; }
        [Column("NoOfInverter_LstJID")]
        public string NoOfInverterLstJid { get; set; }
        [Column("Brand_LstJPD")]
        public string BrandLstJpd { get; set; }
        [Column("Model_LstJPD")]
        public string ModelLstJpd { get; set; }
        [Column("NoOfPanel_LstJPD")]
        public int? NoOfPanelLstJpd { get; set; }
        [Column("STCStatus_JSTCD")]
        public string StcstatusJstcd { get; set; }
        [Column("CalculatedSTC_JSTCD")]
        public string CalculatedStcJstcd { get; set; }
        [Column("STCPrice_JSTCD")]
        public string StcpriceJstcd { get; set; }
        [Column("FailureNotice_JSTCD")]
        public string FailureNoticeJstcd { get; set; }
        [Column("ComplianceNotes_JSTCD")]
        public string ComplianceNotesJstcd { get; set; }
        [Column("STCSubmissionDate_JSTCD")]
        public string StcsubmissionDateJstcd { get; set; }
        [Column("STCInvoiceStatus_JSTCD")]
        public string StcinvoiceStatusJstcd { get; set; }
        [Column("IsInvoiced_JSTCD")]
        public bool IsInvoicedJstcd { get; set; }
        [Column("FieldName_LstCD")]
        public string FieldNameLstCd { get; set; }
        [Column("FieldValue_LstCD")]
        public string FieldValueLstCd { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int? OrganizationId { get; set; }
    }
}
