﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(FriendTenantId), nameof(FriendUserId), Name = "IX_AppFriendships_FriendTenantId_FriendUserId")]
    [Index(nameof(FriendTenantId), nameof(UserId), Name = "IX_AppFriendships_FriendTenantId_UserId")]
    [Index(nameof(TenantId), nameof(FriendUserId), Name = "IX_AppFriendships_TenantId_FriendUserId")]
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AppFriendships_TenantId_UserId")]
    public partial class AppFriendship
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public Guid? FriendProfilePictureId { get; set; }
        public string FriendTenancyName { get; set; }
        public int? FriendTenantId { get; set; }
        public long FriendUserId { get; set; }
        [Required]
        [StringLength(256)]
        public string FriendUserName { get; set; }
        public int State { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
    }
}
