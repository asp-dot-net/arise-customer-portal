﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(PromotionTypeId), Name = "IX_Promotions_PromotionTypeId")]
    [Index(nameof(TenantId), Name = "IX_Promotions_TenantId")]
    public partial class Promotion
    {
        public Promotion()
        {
            PromotionUsers = new HashSet<PromotionUser>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        [Required]
        public string Title { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PromoCharge { get; set; }
        public string Description { get; set; }
        public int? PromotionTypeId { get; set; }
        public int LeadCount { get; set; }
        [Column("OrganizationID")]
        public int OrganizationId { get; set; }
        public string AreaNameFilter { get; set; }
        public DateTime? EndDateFilter { get; set; }
        public string JobStatusIdsFilter { get; set; }
        public string LeadSourceIdsFilter { get; set; }
        public string LeadStatusIdsFilter { get; set; }
        public DateTime? StartDateFilter { get; set; }
        public string StateIdsFilter { get; set; }
        public string TeamIdsFilter { get; set; }
        public string TypeNameFilter { get; set; }

        [ForeignKey(nameof(PromotionTypeId))]
        [InverseProperty("Promotions")]
        public virtual PromotionType PromotionType { get; set; }
        [InverseProperty(nameof(PromotionUser.Promotion))]
        public virtual ICollection<PromotionUser> PromotionUsers { get; set; }
    }
}
