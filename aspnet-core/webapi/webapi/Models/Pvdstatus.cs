﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("PVDStatus")]
    public partial class Pvdstatus
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
