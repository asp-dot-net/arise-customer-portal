﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(EntityChangeSetId), Name = "IX_AbpEntityChanges_EntityChangeSetId")]
    [Index(nameof(EntityTypeFullName), nameof(EntityId), Name = "IX_AbpEntityChanges_EntityTypeFullName_EntityId")]
    public partial class AbpEntityChange
    {
        public AbpEntityChange()
        {
            AbpEntityPropertyChanges = new HashSet<AbpEntityPropertyChange>();
        }

        [Key]
        public long Id { get; set; }
        public DateTime ChangeTime { get; set; }
        public byte ChangeType { get; set; }
        public long EntityChangeSetId { get; set; }
        [StringLength(48)]
        public string EntityId { get; set; }
        [StringLength(192)]
        public string EntityTypeFullName { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(EntityChangeSetId))]
        [InverseProperty(nameof(AbpEntityChangeSet.AbpEntityChanges))]
        public virtual AbpEntityChangeSet EntityChangeSet { get; set; }
        [InverseProperty(nameof(AbpEntityPropertyChange.EntityChange))]
        public virtual ICollection<AbpEntityPropertyChange> AbpEntityPropertyChanges { get; set; }
    }
}
