﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(DynamicParameterId), Name = "IX_AbpEntityDynamicParameters_DynamicParameterId")]
    public partial class AbpEntityDynamicParameter
    {
        public AbpEntityDynamicParameter()
        {
            AbpEntityDynamicParameterValues = new HashSet<AbpEntityDynamicParameterValue>();
        }

        [Key]
        public int Id { get; set; }
        public string EntityFullName { get; set; }
        public int DynamicParameterId { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(DynamicParameterId))]
        [InverseProperty(nameof(AbpDynamicParameter.AbpEntityDynamicParameters))]
        public virtual AbpDynamicParameter DynamicParameter { get; set; }
        [InverseProperty(nameof(AbpEntityDynamicParameterValue.EntityDynamicParameter))]
        public virtual ICollection<AbpEntityDynamicParameterValue> AbpEntityDynamicParameterValues { get; set; }
    }
}
