﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("InstallerAvailability")]
    public partial class InstallerAvailability
    {
        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public long? UserId { get; set; }
        public DateTime AvailabilityDate { get; set; }
    }
}
