﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("LeadAction")]
    public partial class LeadAction
    {
        public LeadAction()
        {
            LeadActivityLogs = new HashSet<LeadActivityLog>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public string ActionName { get; set; }

        [InverseProperty(nameof(LeadActivityLog.LeadAction))]
        public virtual ICollection<LeadActivityLog> LeadActivityLogs { get; set; }
    }
}
