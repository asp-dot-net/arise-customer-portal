﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("InstallerInvoiceHistory")]
    public partial class InstallerInvoiceHistory
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public string FieldName { get; set; }
        public string PrevValue { get; set; }
        public string CurValue { get; set; }
        public string Action { get; set; }
        public DateTime LastmodifiedDateTime { get; set; }
        [Column("jobid")]
        public int? Jobid { get; set; }
        public int? InvoiceInstallerId { get; set; }
    }
}
