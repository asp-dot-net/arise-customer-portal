﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("State", Schema = "HangFire")]
    public partial class State1
    {
        [Key]
        public long Id { get; set; }
        [Key]
        public long JobId { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Reason { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }
        public string Data { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty(nameof(Job1.State1s))]
        public virtual Job1 Job { get; set; }
    }
}
