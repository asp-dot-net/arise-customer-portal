﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class ElecRetailer
    {
        public ElecRetailer()
        {
            Jobs = new HashSet<Job>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public string Name { get; set; }
        [Column("NSW")]
        public bool Nsw { get; set; }
        [Column("SA")]
        public bool Sa { get; set; }
        [Column("QLD")]
        public bool Qld { get; set; }
        [Column("VIC")]
        public bool Vic { get; set; }
        [Column("WA")]
        public bool Wa { get; set; }
        [Column("ACT")]
        public bool Act { get; set; }
        [Column("TAS")]
        public bool Tas { get; set; }
        [Column("NT")]
        public bool Nt { get; set; }
        public int? ElectricityProviderId { get; set; }

        [InverseProperty(nameof(Job.ElecRetailer))]
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
