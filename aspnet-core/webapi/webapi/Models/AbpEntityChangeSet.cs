﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(CreationTime), Name = "IX_AbpEntityChangeSets_TenantId_CreationTime")]
    [Index(nameof(TenantId), nameof(Reason), Name = "IX_AbpEntityChangeSets_TenantId_Reason")]
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AbpEntityChangeSets_TenantId_UserId")]
    public partial class AbpEntityChangeSet
    {
        public AbpEntityChangeSet()
        {
            AbpEntityChanges = new HashSet<AbpEntityChange>();
        }

        [Key]
        public long Id { get; set; }
        [StringLength(512)]
        public string BrowserInfo { get; set; }
        [StringLength(64)]
        public string ClientIpAddress { get; set; }
        [StringLength(128)]
        public string ClientName { get; set; }
        public DateTime CreationTime { get; set; }
        public string ExtensionData { get; set; }
        public int? ImpersonatorTenantId { get; set; }
        public long? ImpersonatorUserId { get; set; }
        [StringLength(256)]
        public string Reason { get; set; }
        public int? TenantId { get; set; }
        public long? UserId { get; set; }

        [InverseProperty(nameof(AbpEntityChange.EntityChangeSet))]
        public virtual ICollection<AbpEntityChange> AbpEntityChanges { get; set; }
    }
}
