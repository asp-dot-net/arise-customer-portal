﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(CreationTime), Name = "IX_AbpTenants_CreationTime")]
    [Index(nameof(CreatorUserId), Name = "IX_AbpTenants_CreatorUserId")]
    [Index(nameof(DeleterUserId), Name = "IX_AbpTenants_DeleterUserId")]
    [Index(nameof(EditionId), Name = "IX_AbpTenants_EditionId")]
    [Index(nameof(LastModifierUserId), Name = "IX_AbpTenants_LastModifierUserId")]
    [Index(nameof(SubscriptionEndDateUtc), Name = "IX_AbpTenants_SubscriptionEndDateUtc")]
    [Index(nameof(TenancyName), Name = "IX_AbpTenants_TenancyName")]
    public partial class AbpTenant
    {
        [Key]
        public int Id { get; set; }
        [StringLength(1024)]
        public string ConnectionString { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public Guid? CustomCssId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? EditionId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        [StringLength(64)]
        public string LogoFileType { get; set; }
        public Guid? LogoId { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [Required]
        [StringLength(64)]
        public string TenancyName { get; set; }
        [Required]
        public bool? IsInTrialPeriod { get; set; }
        public DateTime? SubscriptionEndDateUtc { get; set; }
        public int SubscriptionPaymentType { get; set; }

        [ForeignKey(nameof(CreatorUserId))]
        [InverseProperty(nameof(AbpUser.AbpTenantCreatorUsers))]
        public virtual AbpUser CreatorUser { get; set; }
        [ForeignKey(nameof(DeleterUserId))]
        [InverseProperty(nameof(AbpUser.AbpTenantDeleterUsers))]
        public virtual AbpUser DeleterUser { get; set; }
        [ForeignKey(nameof(EditionId))]
        [InverseProperty(nameof(AbpEdition.AbpTenants))]
        public virtual AbpEdition Edition { get; set; }
        [ForeignKey(nameof(LastModifierUserId))]
        [InverseProperty(nameof(AbpUser.AbpTenantLastModifierUsers))]
        public virtual AbpUser LastModifierUser { get; set; }
    }
}
