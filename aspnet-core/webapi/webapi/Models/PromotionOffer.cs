﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class PromotionOffer
    {
        public PromotionOffer()
        {
            Jobs = new HashSet<Job>();
        }

        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }

        [InverseProperty(nameof(Job.PromotionOffer))]
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
