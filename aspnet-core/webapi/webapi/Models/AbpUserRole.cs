﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(RoleId), Name = "IX_AbpUserRoles_TenantId_RoleId")]
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AbpUserRoles_TenantId_UserId")]
    [Index(nameof(UserId), Name = "IX_AbpUserRoles_UserId")]
    public partial class AbpUserRole
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int RoleId { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpUserRoles))]
        public virtual AbpUser User { get; set; }
    }
}
