﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class AbpWebhookEvent
    {
        public AbpWebhookEvent()
        {
            AbpWebhookSendAttempts = new HashSet<AbpWebhookSendAttempt>();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        public string WebhookName { get; set; }
        public string Data { get; set; }
        public DateTime CreationTime { get; set; }
        public int? TenantId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTime { get; set; }

        [InverseProperty(nameof(AbpWebhookSendAttempt.WebhookEvent))]
        public virtual ICollection<AbpWebhookSendAttempt> AbpWebhookSendAttempts { get; set; }
    }
}
