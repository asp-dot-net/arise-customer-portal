﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class ApplicationSetting
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public bool FoneDynamicsEnabled { get; set; }
        [StringLength(15)]
        public string FoneDynamicsPhoneNumber { get; set; }
        [StringLength(50)]
        public string FoneDynamicsPropertySid { get; set; }
        [StringLength(50)]
        public string FoneDynamicsAccountSid { get; set; }
        public string FoneDynamicsToken { get; set; }
    }
}
