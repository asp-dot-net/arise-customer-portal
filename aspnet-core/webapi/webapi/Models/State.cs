﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("State")]
    public partial class State
    {
        public State()
        {
            LeadExpenses = new HashSet<LeadExpense>();
            PostCodes = new HashSet<PostCode>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public bool? IsReport { get; set; }

        [InverseProperty(nameof(LeadExpense.State))]
        public virtual ICollection<LeadExpense> LeadExpenses { get; set; }
        [InverseProperty(nameof(PostCode.State))]
        public virtual ICollection<PostCode> PostCodes { get; set; }
    }
}
