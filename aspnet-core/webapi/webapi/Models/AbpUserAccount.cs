﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(EmailAddress), Name = "IX_AbpUserAccounts_EmailAddress")]
    [Index(nameof(TenantId), nameof(EmailAddress), Name = "IX_AbpUserAccounts_TenantId_EmailAddress")]
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AbpUserAccounts_TenantId_UserId")]
    [Index(nameof(TenantId), nameof(UserName), Name = "IX_AbpUserAccounts_TenantId_UserName")]
    [Index(nameof(UserName), Name = "IX_AbpUserAccounts_UserName")]
    public partial class AbpUserAccount
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [StringLength(256)]
        public string EmailAddress { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public long? UserLinkId { get; set; }
        [StringLength(256)]
        public string UserName { get; set; }
    }
}
