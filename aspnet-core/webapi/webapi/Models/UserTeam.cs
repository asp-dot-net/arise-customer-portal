﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TeamId), Name = "IX_UserTeams_TeamId")]
    [Index(nameof(UserId), Name = "IX_UserTeams_UserId")]
    public partial class UserTeam
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? TenantId { get; set; }
        public long? UserId { get; set; }
        public int? TeamId { get; set; }

        [ForeignKey(nameof(TeamId))]
        [InverseProperty("UserTeams")]
        public virtual Team Team { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.UserTeams))]
        public virtual AbpUser User { get; set; }
    }
}
