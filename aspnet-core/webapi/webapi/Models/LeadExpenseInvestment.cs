﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class LeadExpenseInvestment
    {
        public LeadExpenseInvestment()
        {
            LeadExpenses = new HashSet<LeadExpense>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? TenantId { get; set; }
        public int OrganizationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string MonthYear { get; set; }
        public float Investment { get; set; }

        [InverseProperty(nameof(LeadExpense.Leadinvestment))]
        public virtual ICollection<LeadExpense> LeadExpenses { get; set; }
    }
}
