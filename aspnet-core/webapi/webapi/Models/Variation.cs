﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_Variations_TenantId")]
    public partial class Variation
    {
        public Variation()
        {
            JobVariations = new HashSet<JobVariation>();
        }

        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string Action { get; set; }

        [InverseProperty(nameof(JobVariation.Variation))]
        public virtual ICollection<JobVariation> JobVariations { get; set; }
    }
}
