﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(RoleId), Name = "IX_AbpPermissions_RoleId")]
    [Index(nameof(TenantId), nameof(Name), Name = "IX_AbpPermissions_TenantId_Name")]
    [Index(nameof(UserId), Name = "IX_AbpPermissions_UserId")]
    public partial class AbpPermission
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        [Required]
        public string Discriminator { get; set; }
        public bool IsGranted { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public int? RoleId { get; set; }
        public long? UserId { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty(nameof(AbpRole.AbpPermissions))]
        public virtual AbpRole Role { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpPermissions))]
        public virtual AbpUser User { get; set; }
    }
}
