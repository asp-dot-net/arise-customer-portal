﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(LoginProvider), nameof(ProviderKey), Name = "IX_AbpUserLogins_TenantId_LoginProvider_ProviderKey")]
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AbpUserLogins_TenantId_UserId")]
    [Index(nameof(UserId), Name = "IX_AbpUserLogins_UserId")]
    public partial class AbpUserLogin
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [StringLength(128)]
        public string LoginProvider { get; set; }
        [Required]
        [StringLength(256)]
        public string ProviderKey { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpUserLogins))]
        public virtual AbpUser User { get; set; }
    }
}
