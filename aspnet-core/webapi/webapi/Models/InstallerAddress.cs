﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_InstallerAddresses_TenantId")]
    [Index(nameof(UserId), Name = "IX_InstallerAddresses_UserId")]
    public partial class InstallerAddress
    {
        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Unit { get; set; }
        public string UnitType { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public long? UserId { get; set; }
        public int? StateId { get; set; }
        public string StreetType { get; set; }
        public string IsGoogle { get; set; }
        [Column("latitude")]
        public string Latitude { get; set; }
        [Column("longitude")]
        public string Longitude { get; set; }
        public string Address { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.InstallerAddresses))]
        public virtual AbpUser User { get; set; }
    }
}
