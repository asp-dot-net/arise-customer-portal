﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_PayWayDatas_jobId")]
    public partial class PayWayData
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Column("jobId")]
        public int JobId { get; set; }
        [Column("transactionId")]
        public string TransactionId { get; set; }
        [Column("receiptNumber")]
        public string ReceiptNumber { get; set; }
        [Column("status")]
        public string Status { get; set; }
        [Column("responseCode")]
        public string ResponseCode { get; set; }
        [Column("responseText")]
        public string ResponseText { get; set; }
        [Column("transactionType")]
        public string TransactionType { get; set; }
        [Column("customerNumber")]
        public string CustomerNumber { get; set; }
        [Column("customerName")]
        public string CustomerName { get; set; }
        [Column("currency")]
        public string Currency { get; set; }
        [Column("principalAmount", TypeName = "decimal(18, 2)")]
        public decimal? PrincipalAmount { get; set; }
        [Column("surchargeAmount", TypeName = "decimal(18, 2)")]
        public decimal? SurchargeAmount { get; set; }
        [Column("paymentAmount", TypeName = "decimal(18, 2)")]
        public decimal? PaymentAmount { get; set; }
        [Column("paymentMethod")]
        public string PaymentMethod { get; set; }
        public int? PaymentType { get; set; }
        public int? OrganizationId { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("PayWayData")]
        public virtual Job Job { get; set; }
    }
}
