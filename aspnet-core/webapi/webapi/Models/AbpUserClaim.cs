﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(ClaimType), Name = "IX_AbpUserClaims_TenantId_ClaimType")]
    [Index(nameof(UserId), Name = "IX_AbpUserClaims_UserId")]
    public partial class AbpUserClaim
    {
        [Key]
        public long Id { get; set; }
        [StringLength(256)]
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpUserClaims))]
        public virtual AbpUser User { get; set; }
    }
}
