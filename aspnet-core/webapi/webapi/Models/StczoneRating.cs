﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("STCZoneRatings")]
    public partial class StczoneRating
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "decimal(8, 3)")]
        public decimal Rating { get; set; }
        public string UpSizeTs { get; set; }
    }
}
