﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(EditionId), nameof(Name), Name = "IX_AbpFeatures_EditionId_Name")]
    [Index(nameof(TenantId), nameof(Name), Name = "IX_AbpFeatures_TenantId_Name")]
    public partial class AbpFeature
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        [Required]
        public string Discriminator { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [Required]
        [StringLength(2000)]
        public string Value { get; set; }
        public int? EditionId { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(EditionId))]
        [InverseProperty(nameof(AbpEdition.AbpFeatures))]
        public virtual AbpEdition Edition { get; set; }
    }
}
