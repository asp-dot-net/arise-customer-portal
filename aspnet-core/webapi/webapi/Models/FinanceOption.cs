﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_FinanceOptions_TenantId")]
    public partial class FinanceOption
    {
        public FinanceOption()
        {
            Jobs = new HashSet<Job>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        public int? DisplayOrder { get; set; }

        [InverseProperty(nameof(Job.FinanceOption))]
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
