﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_AppBinaryObjects_TenantId")]
    public partial class AppBinaryObject
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte[] Bytes { get; set; }
        public int? TenantId { get; set; }
    }
}
