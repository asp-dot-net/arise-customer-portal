﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_InvoiceStatuses_TenantId")]
    public partial class InvoiceStatus
    {
        public InvoiceStatus()
        {
            InvoicePayments = new HashSet<InvoicePayment>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        [Required]
        public string Name { get; set; }

        [InverseProperty(nameof(InvoicePayment.InvoicePaymentStatus))]
        public virtual ICollection<InvoicePayment> InvoicePayments { get; set; }
    }
}
