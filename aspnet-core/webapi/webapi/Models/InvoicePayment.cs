﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(InvoicePaymentMethodId), Name = "IX_InvoicePayments_InvoicePaymentMethodId")]
    [Index(nameof(InvoicePaymentStatusId), Name = "IX_InvoicePayments_InvoicePaymentStatusId")]
    [Index(nameof(JobId), Name = "IX_InvoicePayments_JobId")]
    [Index(nameof(RefundBy), Name = "IX_InvoicePayments_RefundBy")]
    [Index(nameof(TenantId), Name = "IX_InvoicePayments_TenantId")]
    [Index(nameof(UserId), Name = "IX_InvoicePayments_UserId")]
    [Index(nameof(VerifiedBy), Name = "IX_InvoicePayments_VerifiedBy")]
    public partial class InvoicePayment
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        [Column("InvoicePayExGST", TypeName = "decimal(18, 2)")]
        public decimal? InvoicePayExGst { get; set; }
        [Column("InvoicePayGST", TypeName = "decimal(18, 2)")]
        public decimal? InvoicePayGst { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? InvoicePayTotal { get; set; }
        public DateTime? InvoicePayDate { get; set; }
        [Column("CCSurcharge", TypeName = "decimal(18, 2)")]
        public decimal? Ccsurcharge { get; set; }
        public DateTime? VerifiedOn { get; set; }
        [StringLength(255)]
        public string PaymentNote { get; set; }
        public bool Refund { get; set; }
        public int? RefundType { get; set; }
        public DateTime? RefundDate { get; set; }
        public int? PaymentNumber { get; set; }
        public bool IsVerified { get; set; }
        public string ReceiptNumber { get; set; }
        public DateTime? ActualPayDate { get; set; }
        public string PaidComment { get; set; }
        public int? JobId { get; set; }
        public long? UserId { get; set; }
        public long? VerifiedBy { get; set; }
        public long? RefundBy { get; set; }
        public int? InvoicePaymentMethodId { get; set; }
        public int? InvoiceNo { get; set; }
        public int? InvoicePaymentStatusId { get; set; }
        public bool? InvoiceEmailSend { get; set; }
        public DateTime? InvoiceEmailSendDate { get; set; }
        public bool? InvoiceSmsSend { get; set; }
        public DateTime? InvoiceSmsSendDate { get; set; }
        public string TransactionCode { get; set; }

        [ForeignKey(nameof(InvoicePaymentMethodId))]
        [InverseProperty("InvoicePayments")]
        public virtual InvoicePaymentMethod InvoicePaymentMethod { get; set; }
        [ForeignKey(nameof(InvoicePaymentStatusId))]
        [InverseProperty(nameof(InvoiceStatus.InvoicePayments))]
        public virtual InvoiceStatus InvoicePaymentStatus { get; set; }
        [ForeignKey(nameof(JobId))]
        [InverseProperty("InvoicePayments")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(RefundBy))]
        [InverseProperty(nameof(AbpUser.InvoicePaymentRefundByNavigations))]
        public virtual AbpUser RefundByNavigation { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.InvoicePaymentUsers))]
        public virtual AbpUser User { get; set; }
        [ForeignKey(nameof(VerifiedBy))]
        [InverseProperty(nameof(AbpUser.InvoicePaymentVerifiedByNavigations))]
        public virtual AbpUser VerifiedByNavigation { get; set; }
    }
}
