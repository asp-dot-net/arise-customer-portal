﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(RoleId), Name = "IX_AbpRoleClaims_RoleId")]
    [Index(nameof(TenantId), nameof(ClaimType), Name = "IX_AbpRoleClaims_TenantId_ClaimType")]
    public partial class AbpRoleClaim
    {
        [Key]
        public long Id { get; set; }
        [StringLength(256)]
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int RoleId { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty(nameof(AbpRole.AbpRoleClaims))]
        public virtual AbpRole Role { get; set; }
    }
}
