﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace webapi.Models
{
    public partial class DatabaseDbContext : DbContext
    {
        public DatabaseDbContext()
        {
        }

        public DatabaseDbContext(DbContextOptions<DatabaseDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AbpAuditLog> AbpAuditLogs { get; set; }
        public virtual DbSet<AbpBackgroundJob> AbpBackgroundJobs { get; set; }
        public virtual DbSet<AbpDynamicParameter> AbpDynamicParameters { get; set; }
        public virtual DbSet<AbpDynamicParameterValue> AbpDynamicParameterValues { get; set; }
        public virtual DbSet<AbpEdition> AbpEditions { get; set; }
        public virtual DbSet<AbpEntityChange> AbpEntityChanges { get; set; }
        public virtual DbSet<AbpEntityChangeSet> AbpEntityChangeSets { get; set; }
        public virtual DbSet<AbpEntityDynamicParameter> AbpEntityDynamicParameters { get; set; }
        public virtual DbSet<AbpEntityDynamicParameterValue> AbpEntityDynamicParameterValues { get; set; }
        public virtual DbSet<AbpEntityPropertyChange> AbpEntityPropertyChanges { get; set; }
        public virtual DbSet<AbpFeature> AbpFeatures { get; set; }
        public virtual DbSet<AbpLanguage> AbpLanguages { get; set; }
        public virtual DbSet<AbpLanguageText> AbpLanguageTexts { get; set; }
        public virtual DbSet<AbpNotification> AbpNotifications { get; set; }
        public virtual DbSet<AbpNotificationSubscription> AbpNotificationSubscriptions { get; set; }
        public virtual DbSet<AbpOrganizationUnit> AbpOrganizationUnits { get; set; }
        public virtual DbSet<AbpOrganizationUnitRole> AbpOrganizationUnitRoles { get; set; }
        public virtual DbSet<AbpPermission> AbpPermissions { get; set; }
        public virtual DbSet<AbpPersistedGrant> AbpPersistedGrants { get; set; }
        public virtual DbSet<AbpRole> AbpRoles { get; set; }
        public virtual DbSet<AbpRoleClaim> AbpRoleClaims { get; set; }
        public virtual DbSet<AbpSetting> AbpSettings { get; set; }
        public virtual DbSet<AbpTenant> AbpTenants { get; set; }
        public virtual DbSet<AbpTenantNotification> AbpTenantNotifications { get; set; }
        public virtual DbSet<AbpUser> AbpUsers { get; set; }
        public virtual DbSet<AbpUserAccount> AbpUserAccounts { get; set; }
        public virtual DbSet<AbpUserClaim> AbpUserClaims { get; set; }
        public virtual DbSet<AbpUserLogin> AbpUserLogins { get; set; }
        public virtual DbSet<AbpUserLoginAttempt> AbpUserLoginAttempts { get; set; }
        public virtual DbSet<AbpUserNotification> AbpUserNotifications { get; set; }
        public virtual DbSet<AbpUserOrganizationUnit> AbpUserOrganizationUnits { get; set; }
        public virtual DbSet<AbpUserRole> AbpUserRoles { get; set; }
        public virtual DbSet<AbpUserToken> AbpUserTokens { get; set; }
        public virtual DbSet<AbpWebhookEvent> AbpWebhookEvents { get; set; }
        public virtual DbSet<AbpWebhookSendAttempt> AbpWebhookSendAttempts { get; set; }
        public virtual DbSet<AbpWebhookSubscription> AbpWebhookSubscriptions { get; set; }
        public virtual DbSet<AggregatedCounter> AggregatedCounters { get; set; }
        public virtual DbSet<AppBinaryObject> AppBinaryObjects { get; set; }
        public virtual DbSet<AppChatMessage> AppChatMessages { get; set; }
        public virtual DbSet<AppFriendship> AppFriendships { get; set; }
        public virtual DbSet<AppInvoice> AppInvoices { get; set; }
        public virtual DbSet<AppSubscriptionPayment> AppSubscriptionPayments { get; set; }
        public virtual DbSet<AppSubscriptionPaymentsExtensionDatum> AppSubscriptionPaymentsExtensionData { get; set; }
        public virtual DbSet<AppUserDelegation> AppUserDelegations { get; set; }
        public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public virtual DbSet<Calender> Calenders { get; set; }
        public virtual DbSet<CancelReason> CancelReasons { get; set; }
        public virtual DbSet<CasualMaintenance> CasualMaintenances { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Counter> Counters { get; set; }
        public virtual DbSet<CpReferelDetail> CpReferelDetails { get; set; }
        public virtual DbSet<DepositOption> DepositOptions { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentLibrary> DocumentLibrarys { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<ElecDistributor> ElecDistributors { get; set; }
        public virtual DbSet<ElecRetailer> ElecRetailers { get; set; }
        public virtual DbSet<EmailTempData> EmailTempDatas { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<FinanceOption> FinanceOptions { get; set; }
        public virtual DbSet<FreebieTransport> FreebieTransports { get; set; }
        public virtual DbSet<GreenBoatData> GreenBoatDatas { get; set; }
        public virtual DbSet<GreenBoatSerialNumber> GreenBoatSerialNumbers { get; set; }
        public virtual DbSet<Hash> Hashes { get; set; }
        public virtual DbSet<HoldReason> HoldReasons { get; set; }
        public virtual DbSet<HouseType> HouseTypes { get; set; }
        public virtual DbSet<InstallerAddress> InstallerAddresses { get; set; }
        public virtual DbSet<InstallerAvailability> InstallerAvailabilities { get; set; }
        public virtual DbSet<InstallerContract> InstallerContracts { get; set; }
        public virtual DbSet<InstallerDetail> InstallerDetails { get; set; }
        public virtual DbSet<InstallerInvoice> InstallerInvoices { get; set; }
        public virtual DbSet<InstallerInvoiceHistory> InstallerInvoiceHistories { get; set; }
        public virtual DbSet<InvoiceFile> InvoiceFiles { get; set; }
        public virtual DbSet<InvoiceImportData> InvoiceImportDatas { get; set; }
        public virtual DbSet<InvoicePayment> InvoicePayments { get; set; }
        public virtual DbSet<InvoicePaymentMethod> InvoicePaymentMethods { get; set; }
        public virtual DbSet<InvoiceStatus> InvoiceStatuses { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<Job1> Jobs1 { get; set; }
        public virtual DbSet<JobApprovalFileHistory> JobApprovalFileHistories { get; set; }
        public virtual DbSet<JobCancellationReason> JobCancellationReasons { get; set; }
        public virtual DbSet<JobOldSystemDetail> JobOldSystemDetails { get; set; }
        public virtual DbSet<JobParameter> JobParameters { get; set; }
        public virtual DbSet<JobProductItem> JobProductItems { get; set; }
        public virtual DbSet<JobPromotion> JobPromotions { get; set; }
        public virtual DbSet<JobQueue> JobQueues { get; set; }
        public virtual DbSet<JobRefund> JobRefunds { get; set; }
        public virtual DbSet<JobReview> JobReviews { get; set; }
        public virtual DbSet<JobStatus> JobStatuses { get; set; }
        public virtual DbSet<JobTrackerHistory> JobTrackerHistories { get; set; }
        public virtual DbSet<JobType> JobTypes { get; set; }
        public virtual DbSet<JobVariation> JobVariations { get; set; }
        public virtual DbSet<Jobswarranty> Jobswarranties { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }
        public virtual DbSet<LeadAction> LeadActions { get; set; }
        public virtual DbSet<LeadActivityLog> LeadActivityLogs { get; set; }
        public virtual DbSet<LeadExpense> LeadExpenses { get; set; }
        public virtual DbSet<LeadExpenseInvestment> LeadExpenseInvestments { get; set; }
        public virtual DbSet<LeadSource> LeadSources { get; set; }
        public virtual DbSet<LeadStatus> LeadStatuses { get; set; }
        public virtual DbSet<LeadtrackerHistory> LeadtrackerHistories { get; set; }
        public virtual DbSet<List> Lists { get; set; }
        public virtual DbSet<MeterPhase> MeterPhases { get; set; }
        public virtual DbSet<MeterUpgrade> MeterUpgrades { get; set; }
        public virtual DbSet<PayWayData> PayWayDatas { get; set; }
        public virtual DbSet<PaymentOption> PaymentOptions { get; set; }
        public virtual DbSet<PostCode> PostCodes { get; set; }
        public virtual DbSet<PostalType> PostalTypes { get; set; }
        public virtual DbSet<PreviousJobStatus> PreviousJobStatuses { get; set; }
        public virtual DbSet<ProductItem> ProductItems { get; set; }
        public virtual DbSet<ProductItemLocation> ProductItemLocations { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<PromotionMaster> PromotionMasters { get; set; }
        public virtual DbSet<PromotionOffer> PromotionOffers { get; set; }
        public virtual DbSet<PromotionResponseStatus> PromotionResponseStatuses { get; set; }
        public virtual DbSet<PromotionType> PromotionTypes { get; set; }
        public virtual DbSet<PromotionUser> PromotionUsers { get; set; }
        public virtual DbSet<Pvdstatus> Pvdstatuses { get; set; }
        public virtual DbSet<Quotation> Quotations { get; set; }
        public virtual DbSet<QuotationLinkHistory> QuotationLinkHistorys { get; set; }
        public virtual DbSet<RefundReason> RefundReasons { get; set; }
        public virtual DbSet<RejectReason> RejectReasons { get; set; }
        public virtual DbSet<ReviewType> ReviewTypes { get; set; }
        public virtual DbSet<RoofAngle> RoofAngles { get; set; }
        public virtual DbSet<RoofType> RoofTypes { get; set; }
        public virtual DbSet<Schema> Schemas { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<Server> Servers { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceCategory> ServiceCategorys { get; set; }
        public virtual DbSet<ServiceCategoryDoc> ServiceCategoryDocs { get; set; }
        public virtual DbSet<ServiceDoc> ServiceDocs { get; set; }
        public virtual DbSet<ServicePriority> ServicePrioritys { get; set; }
        public virtual DbSet<ServiceSource> ServiceSources { get; set; }
        public virtual DbSet<ServiceStatus> ServiceStatuses { get; set; }
        public virtual DbSet<ServiceSubCategory> ServiceSubCategorys { get; set; }
        public virtual DbSet<ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<Set> Sets { get; set; }
        public virtual DbSet<SmsTemplate> SmsTemplates { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<State1> States1 { get; set; }
        public virtual DbSet<StcpostalCode> StcpostalCodes { get; set; }
        public virtual DbSet<StcyearWiseRate> StcyearWiseRates { get; set; }
        public virtual DbSet<StczoneRating> StczoneRatings { get; set; }
        public virtual DbSet<StreetName> StreetNames { get; set; }
        public virtual DbSet<StreetType> StreetTypes { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<Temp> Temps { get; set; }
        public virtual DbSet<UnitType> UnitTypes { get; set; }
        public virtual DbSet<UserTeam> UserTeams { get; set; }
        public virtual DbSet<UserWiseEmailOrg> UserWiseEmailOrgs { get; set; }
        public virtual DbSet<Variation> Variations { get; set; }
        public virtual DbSet<WareHouseLocation> WareHouseLocations { get; set; }
        public virtual DbSet<WestPackDatum> WestPackData { get; set; }
        public virtual DbSet<WestPackPrincipalAmount> WestPackPrincipalAmounts { get; set; }
        public virtual DbSet<WestPackSurchargeAmount> WestPackSurchargeAmounts { get; set; }
        public virtual DbSet<WestPackTotalAmount> WestPackTotalAmounts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=52.140.99.10;Initial Catalog=SP_SolarProduct;User ID=ariseadmin;Password=Google$2020#d;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AbpDynamicParameter>(entity =>
            {
                entity.HasIndex(e => new { e.ParameterName, e.TenantId }, "IX_AbpDynamicParameters_ParameterName_TenantId")
                    .IsUnique()
                    .HasFilter("([ParameterName] IS NOT NULL AND [TenantId] IS NOT NULL)");
            });

            modelBuilder.Entity<AbpEdition>(entity =>
            {
                entity.Property(e => e.Discriminator).HasDefaultValueSql("(N'')");
            });

            modelBuilder.Entity<AbpEntityDynamicParameter>(entity =>
            {
                entity.HasIndex(e => new { e.EntityFullName, e.DynamicParameterId, e.TenantId }, "IX_AbpEntityDynamicParameters_EntityFullName_DynamicParameterId_TenantId")
                    .IsUnique()
                    .HasFilter("([EntityFullName] IS NOT NULL AND [TenantId] IS NOT NULL)");
            });

            modelBuilder.Entity<AbpFeature>(entity =>
            {
                entity.HasOne(d => d.Edition)
                    .WithMany(p => p.AbpFeatures)
                    .HasForeignKey(d => d.EditionId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<AbpLanguage>(entity =>
            {
                entity.Property(e => e.IsDisabled).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<AbpNotification>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AbpNotificationSubscription>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AbpOrganizationUnit>(entity =>
            {
                entity.Property(e => e.Discriminator).HasDefaultValueSql("(N'ExtendOrganizationUnit')");
            });

            modelBuilder.Entity<AbpPermission>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AbpPermissions)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AbpPermissions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<AbpTenant>(entity =>
            {
                entity.Property(e => e.IsInTrialPeriod).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<AbpTenantNotification>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AbpUser>(entity =>
            {
                entity.Property(e => e.Ismyinstalleruser).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<AbpUserNotification>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AbpUserOrganizationUnit>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<AbpWebhookEvent>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AbpWebhookSendAttempt>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AbpWebhookSubscription>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AggregatedCounter>(entity =>
            {
                entity.HasKey(e => e.Key)
                    .HasName("PK_HangFire_CounterAggregated");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_AggregatedCounter_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");
            });

            modelBuilder.Entity<AppBinaryObject>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<AppSubscriptionPayment>(entity =>
            {
                entity.Property(e => e.IsRecurring).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<AppSubscriptionPaymentsExtensionDatum>(entity =>
            {
                entity.HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted }, "IX_AppSubscriptionPaymentsExtensionData_SubscriptionPaymentId_Key_IsDeleted")
                    .IsUnique()
                    .HasFilter("([Key] IS NOT NULL)");
            });

            modelBuilder.Entity<Counter>(entity =>
            {
                entity.HasIndex(e => e.Key, "CX_HangFire_Counter")
                    .IsClustered();
            });

            modelBuilder.Entity<CpReferelDetail>(entity =>
            {
                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.FirstName).IsUnicode(false);

                entity.Property(e => e.LastName).IsUnicode(false);

                entity.Property(e => e.Mobile).IsUnicode(false);

                entity.Property(e => e.PropertyAddress).IsUnicode(false);
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.Property(e => e.TemplateName).HasDefaultValueSql("(N'')");
            });

            modelBuilder.Entity<Hash>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Field })
                    .HasName("PK_HangFire_Hash");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_Hash_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.Property(e => e.CreationTime).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("(CONVERT([bit],(0)))");

                entity.Property(e => e.ReferenceJobId).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReferralPayment).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<Job1>(entity =>
            {
                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_Job_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.HasIndex(e => e.StateName, "IX_HangFire_Job_StateName")
                    .HasFilter("([StateName] IS NOT NULL)");
            });

            modelBuilder.Entity<JobParameter>(entity =>
            {
                entity.HasKey(e => new { e.JobId, e.Name })
                    .HasName("PK_HangFire_JobParameter");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.JobParameters)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_HangFire_JobParameter_Job");
            });

            modelBuilder.Entity<JobQueue>(entity =>
            {
                entity.HasKey(e => new { e.Queue, e.Id })
                    .HasName("PK_HangFire_JobQueue");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Lead>(entity =>
            {
                entity.Property(e => e.IsPromotion).HasDefaultValueSql("(CONVERT([bit],(1)))");

                entity.HasOne(d => d.LeadStatus)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.LeadStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<LeadActivityLog>(entity =>
            {
                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.LeadActivityLogs)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<List>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Id })
                    .HasName("PK_HangFire_List");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_List_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ProductItem>(entity =>
            {
                entity.Property(e => e.Active).HasDefaultValueSql("(CONVERT([bit],(0)))");

                entity.Property(e => e.CreationTime).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<ProductItemLocation>(entity =>
            {
                entity.Property(e => e.SalesTag).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<RefundReason>(entity =>
            {
                entity.Property(e => e.CreationTime).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<Schema>(entity =>
            {
                entity.HasKey(e => e.Version)
                    .HasName("PK_HangFire_Schema");

                entity.Property(e => e.Version).ValueGeneratedNever();
            });

            modelBuilder.Entity<Set>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Value })
                    .HasName("PK_HangFire_Set");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_Set_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.Property(e => e.IsReport).HasDefaultValueSql("(CONVERT([bit],(0)))");
            });

            modelBuilder.Entity<State1>(entity =>
            {
                entity.HasKey(e => new { e.JobId, e.Id })
                    .HasName("PK_HangFire_State");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.State1s)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_HangFire_State_Job");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
