﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AbpUserTokens_TenantId_UserId")]
    [Index(nameof(UserId), Name = "IX_AbpUserTokens_UserId")]
    public partial class AbpUserToken
    {
        [Key]
        public long Id { get; set; }
        [StringLength(128)]
        public string LoginProvider { get; set; }
        [StringLength(128)]
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        [StringLength(512)]
        public string Value { get; set; }
        public DateTime? ExpireDate { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpUserTokens))]
        public virtual AbpUser User { get; set; }
    }
}
