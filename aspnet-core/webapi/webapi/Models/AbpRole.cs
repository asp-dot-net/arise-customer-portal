﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(CreatorUserId), Name = "IX_AbpRoles_CreatorUserId")]
    [Index(nameof(DeleterUserId), Name = "IX_AbpRoles_DeleterUserId")]
    [Index(nameof(LastModifierUserId), Name = "IX_AbpRoles_LastModifierUserId")]
    [Index(nameof(TenantId), nameof(NormalizedName), Name = "IX_AbpRoles_TenantId_NormalizedName")]
    public partial class AbpRole
    {
        public AbpRole()
        {
            AbpPermissions = new HashSet<AbpPermission>();
            AbpRoleClaims = new HashSet<AbpRoleClaim>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(128)]
        public string ConcurrencyStamp { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(64)]
        public string DisplayName { get; set; }
        public bool IsDefault { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsStatic { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        [Required]
        [StringLength(32)]
        public string Name { get; set; }
        [Required]
        [StringLength(32)]
        public string NormalizedName { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(CreatorUserId))]
        [InverseProperty(nameof(AbpUser.AbpRoleCreatorUsers))]
        public virtual AbpUser CreatorUser { get; set; }
        [ForeignKey(nameof(DeleterUserId))]
        [InverseProperty(nameof(AbpUser.AbpRoleDeleterUsers))]
        public virtual AbpUser DeleterUser { get; set; }
        [ForeignKey(nameof(LastModifierUserId))]
        [InverseProperty(nameof(AbpUser.AbpRoleLastModifierUsers))]
        public virtual AbpUser LastModifierUser { get; set; }
        [InverseProperty(nameof(AbpPermission.Role))]
        public virtual ICollection<AbpPermission> AbpPermissions { get; set; }
        [InverseProperty(nameof(AbpRoleClaim.Role))]
        public virtual ICollection<AbpRoleClaim> AbpRoleClaims { get; set; }
    }
}
