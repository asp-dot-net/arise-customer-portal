﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(DepositOptionId), Name = "IX_Jobs_DepositOptionId")]
    [Index(nameof(ElecDistributorId), Name = "IX_Jobs_ElecDistributorId")]
    [Index(nameof(ElecRetailerId), Name = "IX_Jobs_ElecRetailerId")]
    [Index(nameof(FinanceOptionId), Name = "IX_Jobs_FinanceOptionId")]
    [Index(nameof(HouseTypeId), Name = "IX_Jobs_HouseTypeId")]
    [Index(nameof(JobStatusId), Name = "IX_Jobs_JobStatusId")]
    [Index(nameof(JobTypeId), Name = "IX_Jobs_JobTypeId")]
    [Index(nameof(LeadId), Name = "IX_Jobs_LeadId")]
    [Index(nameof(PaymentOptionId), Name = "IX_Jobs_PaymentOptionId")]
    [Index(nameof(PromotionOfferId), Name = "IX_Jobs_PromotionOfferId")]
    [Index(nameof(RoofAngleId), Name = "IX_Jobs_RoofAngleId")]
    [Index(nameof(RoofTypeId), Name = "IX_Jobs_RoofTypeId")]
    [Index(nameof(TenantId), Name = "IX_Jobs_TenantId")]
    public partial class Job
    {
        public Job()
        {
            Documents = new HashSet<Document>();
            InstallerInvoices = new HashSet<InstallerInvoice>();
            InvoiceImportData = new HashSet<InvoiceImportData>();
            InvoicePayments = new HashSet<InvoicePayment>();
            JobOldSystemDetails = new HashSet<JobOldSystemDetail>();
            JobProductItems = new HashSet<JobProductItem>();
            JobPromotions = new HashSet<JobPromotion>();
            JobRefunds = new HashSet<JobRefund>();
            JobReviews = new HashSet<JobReview>();
            JobVariations = new HashSet<JobVariation>();
            PayWayData = new HashSet<PayWayData>();
            PreviousJobStatuses = new HashSet<PreviousJobStatus>();
            Quotations = new HashSet<Quotation>();
            Services = new HashSet<Service>();
        }

        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Note { get; set; }
        public int? HouseTypeId { get; set; }
        public int? PanelOnFlat { get; set; }
        public int? PanelOnPitched { get; set; }
        [Column("NMINumber")]
        public string Nminumber { get; set; }
        public string RegPlanNo { get; set; }
        public string LotNumber { get; set; }
        public string PeakMeterNo { get; set; }
        public string OffPeakMeter { get; set; }
        public bool EnoughMeterSpace { get; set; }
        public bool IsSystemOffPeak { get; set; }
        public bool IsFinanceWithUs { get; set; }
        public int? PaymentOptionId { get; set; }
        public int? DepositOptionId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? BasicCost { get; set; }
        [Column("SolarVLCRebate", TypeName = "decimal(18, 2)")]
        public decimal? SolarVlcrebate { get; set; }
        [Column("SolarVLCLoan", TypeName = "decimal(18, 2)")]
        public decimal? SolarVlcloan { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? TotalCost { get; set; }
        public string Suburb { get; set; }
        public int? SuburbId { get; set; }
        public string State { get; set; }
        public int? StateId { get; set; }
        public string PostalCode { get; set; }
        public string UnitNo { get; set; }
        public string UnitType { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? SystemCapacity { get; set; }
        [Column("STC", TypeName = "decimal(18, 2)")]
        public decimal? Stc { get; set; }
        [Column("STCPrice", TypeName = "decimal(18, 2)")]
        public decimal? Stcprice { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Rebate { get; set; }
        public int? JobTypeId { get; set; }
        public int? JobStatusId { get; set; }
        public int? RoofTypeId { get; set; }
        public int? RoofAngleId { get; set; }
        public int? ElecDistributorId { get; set; }
        public int? LeadId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? ElecRetailerId { get; set; }
        [Required]
        public bool? IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public int? MeterPhaseId { get; set; }
        public int? MeterUpgradeId { get; set; }
        public int? PromotionOfferId { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public int? FinanceOptionId { get; set; }
        public string AdditionalComments { get; set; }
        public string ApplicationRefNo { get; set; }
        public DateTime? DistAppliedDate { get; set; }
        public string MeterNumber { get; set; }
        public string InstallerNotes { get; set; }
        public string OldSystemDetails { get; set; }
        public int? DesignerId { get; set; }
        public int? ElectricianId { get; set; }
        public DateTime? InstallationDate { get; set; }
        public string InstallationNotes { get; set; }
        public string InstallationTime { get; set; }
        public int? InstallerId { get; set; }
        public int? WarehouseLocation { get; set; }
        public string JobCancelReason { get; set; }
        public int? JobCancelReasonId { get; set; }
        public string JobHoldReason { get; set; }
        public DateTime? NextFollowUpDate { get; set; }
        public string JobNumber { get; set; }
        public bool? IsRefund { get; set; }
        public int? JobHoldReasonId { get; set; }
        public bool? RefundProcess { get; set; }
        public string ComplianceCertificate { get; set; }
        public string IncompleteReason { get; set; }
        public DateTime? InspectionDate { get; set; }
        public string InspectorName { get; set; }
        public DateTime? InstalledcompleteDate { get; set; }
        public string MeterApplyRef { get; set; }
        public int? PostInstallationStatus { get; set; }
        [Column("PVDNumber")]
        public string Pvdnumber { get; set; }
        [Column("PVDStatus")]
        public int? Pvdstatus { get; set; }
        [Column("STCAppliedDate")]
        public DateTime? StcappliedDate { get; set; }
        [Column("STCNotes")]
        public string Stcnotes { get; set; }
        [Column("STCUploaddate")]
        public DateTime? Stcuploaddate { get; set; }
        public int? Quickformid { get; set; }
        [Column("STCUploadNumber")]
        public string StcuploadNumber { get; set; }
        public TimeSpan? MeterTime { get; set; }
        public DateTime? FinanceApplicationDate { get; set; }
        public int? FinanceAppliedBy { get; set; }
        public int? FinanceDocReceivedBy { get; set; }
        public DateTime? FinanceDocReceivedDate { get; set; }
        public int? FinanceDocSentBy { get; set; }
        public DateTime? FinanceDocSentDate { get; set; }
        public int? FinanceDocumentVerified { get; set; }
        public string FinancePurchaseNo { get; set; }
        public DateTime? ActiveDate { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string RebateAppRef { get; set; }
        public int? SolarRebateStatus { get; set; }
        [Column("SolarVICLoanDiscont", TypeName = "decimal(18, 2)")]
        public decimal? SolarVicloanDiscont { get; set; }
        [Column("SolarVICRebate", TypeName = "decimal(18, 2)")]
        public decimal? SolarVicrebate { get; set; }
        public string VicRebate { get; set; }
        public string VicRebateNotes { get; set; }
        public int? AppliedBy { get; set; }
        public string ApprovalRef { get; set; }
        public DateTime? DistApplied { get; set; }
        public int? DistApproveBy { get; set; }
        public DateTime? DistApproveDate { get; set; }
        public int? EmpId { get; set; }
        public DateTime? InvPaidDate { get; set; }
        public string InvRefNo { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? PaidAmmount { get; set; }
        public int? PaidStatus { get; set; }
        public int? Paidby { get; set; }
        public string ApplicationNotes { get; set; }
        public DateTime? DistExpiryDate { get; set; }
        public string ProjectNotes { get; set; }
        public DateTime? CoolingoffPeriodEnd { get; set; }
        public int? DocumentsVeridiedBy { get; set; }
        public DateTime? DocumentsVeridiedDate { get; set; }
        public string FinanceNotes { get; set; }
        public string ManualQuote { get; set; }
        public DateTime? DepositeRecceivedDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ApprovedCapacityonExport { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ApprovedCapacityonNonExport { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? DepositRequired { get; set; }
        public bool? EmailSend { get; set; }
        public DateTime? EmailSendDate { get; set; }
        public bool? SmsSend { get; set; }
        public DateTime? SmsSendDate { get; set; }
        public bool? FinanceEmailSend { get; set; }
        public DateTime? FinanceEmailSendDate { get; set; }
        public bool? FinanceSmsSend { get; set; }
        public DateTime? FinanceSmsSendDate { get; set; }
        public bool? GridConnectionEmailSend { get; set; }
        public DateTime? GridConnectionEmailSendDate { get; set; }
        public bool? GridConnectionSmsSend { get; set; }
        public DateTime? GridConnectionSmsSendDate { get; set; }
        public bool? JobActiveEmailSend { get; set; }
        public DateTime? JobActiveEmailSendDate { get; set; }
        public bool? JobActiveSmsSend { get; set; }
        public DateTime? JobActiveSmsSendDate { get; set; }
        public bool? StcEmailSend { get; set; }
        public DateTime? StcEmailSendDate { get; set; }
        public bool? StcSmsSend { get; set; }
        public DateTime? StcSmsSendDate { get; set; }
        public bool? JobGridEmailSend { get; set; }
        public DateTime? JobGridEmailSendDate { get; set; }
        public bool? JobGridSmsSend { get; set; }
        public DateTime? JobGridSmsSendDate { get; set; }
        public string JobCancelRequestReason { get; set; }
        public bool? IsJobCancelRequest { get; set; }
        public string JobCancelRejectReason { get; set; }
        public string GridConnectionNotes { get; set; }
        public int? BookingManagerId { get; set; }
        [Column("ApprovalLetter_FilePath")]
        public string ApprovalLetterFilePath { get; set; }
        [Column("ApprovalLetter_Filename")]
        public string ApprovalLetterFilename { get; set; }
        public int? Applicationfeespaid { get; set; }
        public DateTime? JobAssignDate { get; set; }
        public bool? PendingInstallerEmailSend { get; set; }
        public DateTime? PendingInstallerEmailSendDate { get; set; }
        public bool? PendingInstallerSmsSend { get; set; }
        public DateTime? PendingInstallerSmsSendDate { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string BsbNo { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ReferralAmount { get; set; }
        public int? RefferedJobStatusId { get; set; }
        public int? RefferedJobId { get; set; }
        public DateTime? ReferralPayDate { get; set; }
        [Required]
        public bool? ReferralPayment { get; set; }
        public string BankReferenceNo { get; set; }
        public string ReferralRemark { get; set; }
        public bool? WarrantyEmailSend { get; set; }
        public DateTime? WarrantyEmailSendDate { get; set; }
        public bool? WarrantySmsSend { get; set; }
        public DateTime? WarrantySmsSendDate { get; set; }
        public int? RemoveOldSys { get; set; }
        public bool? IsGreenBoatSaved { get; set; }
        public bool? IsRefferalVerify { get; set; }
        public string ActiveStatusNotes { get; set; }
        public string DepositeStatusNotes { get; set; }
        public string ReviewNotes { get; set; }
        public int? ReviewRating { get; set; }
        public int? ReviewAssignId { get; set; }
        public DateTime? ReviewAssignDate { get; set; }
        public int? ReviewType { get; set; }
        public bool? ReviewEmailSend { get; set; }
        public DateTime? ReviewEmailSendDate { get; set; }
        public bool? ReviewSmsSend { get; set; }
        public DateTime? ReviewSmsSendDate { get; set; }
        public int? ReferenceJobId { get; set; }

        [ForeignKey(nameof(DepositOptionId))]
        [InverseProperty("Jobs")]
        public virtual DepositOption DepositOption { get; set; }
        [ForeignKey(nameof(ElecDistributorId))]
        [InverseProperty("Jobs")]
        public virtual ElecDistributor ElecDistributor { get; set; }
        [ForeignKey(nameof(ElecRetailerId))]
        [InverseProperty("Jobs")]
        public virtual ElecRetailer ElecRetailer { get; set; }
        [ForeignKey(nameof(FinanceOptionId))]
        [InverseProperty("Jobs")]
        public virtual FinanceOption FinanceOption { get; set; }
        [ForeignKey(nameof(HouseTypeId))]
        [InverseProperty("Jobs")]
        public virtual HouseType HouseType { get; set; }
        [ForeignKey(nameof(JobStatusId))]
        [InverseProperty("Jobs")]
        public virtual JobStatus JobStatus { get; set; }
        [ForeignKey(nameof(JobTypeId))]
        [InverseProperty("Jobs")]
        public virtual JobType JobType { get; set; }
        [ForeignKey(nameof(LeadId))]
        [InverseProperty("Jobs")]
        public virtual Lead Lead { get; set; }
        [ForeignKey(nameof(PaymentOptionId))]
        [InverseProperty("Jobs")]
        public virtual PaymentOption PaymentOption { get; set; }
        [ForeignKey(nameof(PromotionOfferId))]
        [InverseProperty("Jobs")]
        public virtual PromotionOffer PromotionOffer { get; set; }
        [ForeignKey(nameof(RoofAngleId))]
        [InverseProperty("Jobs")]
        public virtual RoofAngle RoofAngle { get; set; }
        [ForeignKey(nameof(RoofTypeId))]
        [InverseProperty("Jobs")]
        public virtual RoofType RoofType { get; set; }
        [InverseProperty(nameof(Document.Job))]
        public virtual ICollection<Document> Documents { get; set; }
        [InverseProperty(nameof(InstallerInvoice.Job))]
        public virtual ICollection<InstallerInvoice> InstallerInvoices { get; set; }
        [InverseProperty("Job")]
        public virtual ICollection<InvoiceImportData> InvoiceImportData { get; set; }
        [InverseProperty(nameof(InvoicePayment.Job))]
        public virtual ICollection<InvoicePayment> InvoicePayments { get; set; }
        [InverseProperty(nameof(JobOldSystemDetail.Job))]
        public virtual ICollection<JobOldSystemDetail> JobOldSystemDetails { get; set; }
        [InverseProperty(nameof(JobProductItem.Job))]
        public virtual ICollection<JobProductItem> JobProductItems { get; set; }
        [InverseProperty(nameof(JobPromotion.Job))]
        public virtual ICollection<JobPromotion> JobPromotions { get; set; }
        [InverseProperty(nameof(JobRefund.Job))]
        public virtual ICollection<JobRefund> JobRefunds { get; set; }
        [InverseProperty(nameof(JobReview.Job))]
        public virtual ICollection<JobReview> JobReviews { get; set; }
        [InverseProperty(nameof(JobVariation.Job))]
        public virtual ICollection<JobVariation> JobVariations { get; set; }
        [InverseProperty("Job")]
        public virtual ICollection<PayWayData> PayWayData { get; set; }
        [InverseProperty(nameof(PreviousJobStatus.Job))]
        public virtual ICollection<PreviousJobStatus> PreviousJobStatuses { get; set; }
        [InverseProperty(nameof(Quotation.Job))]
        public virtual ICollection<Quotation> Quotations { get; set; }
        [InverseProperty(nameof(Service.Job))]
        public virtual ICollection<Service> Services { get; set; }
    }
}
