﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(Name), nameof(UserId), Name = "IX_AbpSettings_TenantId_Name_UserId", IsUnique = true)]
    [Index(nameof(UserId), Name = "IX_AbpSettings_UserId")]
    public partial class AbpSetting
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        [Required]
        [StringLength(256)]
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public long? UserId { get; set; }
        public string Value { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpSettings))]
        public virtual AbpUser User { get; set; }
    }
}
