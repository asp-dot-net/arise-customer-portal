﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(DynamicParameterId), Name = "IX_AbpDynamicParameterValues_DynamicParameterId")]
    public partial class AbpDynamicParameterValue
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Value { get; set; }
        public int? TenantId { get; set; }
        public int DynamicParameterId { get; set; }

        [ForeignKey(nameof(DynamicParameterId))]
        [InverseProperty(nameof(AbpDynamicParameter.AbpDynamicParameterValues))]
        public virtual AbpDynamicParameter DynamicParameter { get; set; }
    }
}
