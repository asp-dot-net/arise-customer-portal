﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("STCYearWiseRates")]
    public partial class StcyearWiseRate
    {
        [Key]
        public int Id { get; set; }
        public int Year { get; set; }
        public int Rate { get; set; }
    }
}
