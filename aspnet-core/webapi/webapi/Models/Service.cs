﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(JobId), Name = "IX_Services_JobId")]
    [Index(nameof(LeadId), Name = "IX_Services_LeadId")]
    [Index(nameof(ServicePriorityId), Name = "IX_Services_ServicePriorityId")]
    [Index(nameof(ServiceStatusId), Name = "IX_Services_ServiceStatusId")]
    [Index(nameof(ServiceSubCategoryId), Name = "IX_Services_ServiceSubCategoryId")]
    public partial class Service
    {
        public Service()
        {
            ServiceDocs = new HashSet<ServiceDoc>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int OrganizationId { get; set; }
        public string State { get; set; }
        public int? ServicePriorityId { get; set; }
        public int? ServiceCategoryId { get; set; }
        public int? ServiceStatusId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string JobNumber { get; set; }
        public string Notes { get; set; }
        public DateTime? ServiceAssignDate { get; set; }
        public string ServiceLine { get; set; }
        public int? LeadId { get; set; }
        public string ServiceCategoryName { get; set; }
        public int? ServiceAssignId { get; set; }
        public int? JobId { get; set; }
        public string ServiceSource { get; set; }
        public int? IsExternalLead { get; set; }
        public bool? IsDuplicate { get; set; }
        public bool? IsMergeRecord { get; set; }
        public int? IsMergeCount { get; set; }
        public int? ServiceSubCategoryId { get; set; }
        public string OtherEmail { get; set; }
        public DateTime? InstallerDate { get; set; }
        public int? InstallerId { get; set; }
        public string InstallerNotes { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty("Services")]
        public virtual Job Job { get; set; }
        [ForeignKey(nameof(LeadId))]
        [InverseProperty("Services")]
        public virtual Lead Lead { get; set; }
        [ForeignKey(nameof(ServicePriorityId))]
        [InverseProperty("Services")]
        public virtual ServicePriority ServicePriority { get; set; }
        [ForeignKey(nameof(ServiceStatusId))]
        [InverseProperty("Services")]
        public virtual ServiceStatus ServiceStatus { get; set; }
        [ForeignKey(nameof(ServiceSubCategoryId))]
        [InverseProperty("Services")]
        public virtual ServiceSubCategory ServiceSubCategory { get; set; }
        [InverseProperty(nameof(ServiceDoc.Service))]
        public virtual ICollection<ServiceDoc> ServiceDocs { get; set; }
    }
}
