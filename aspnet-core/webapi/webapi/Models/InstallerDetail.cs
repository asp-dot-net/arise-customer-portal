﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_InstallerDetails_TenantId")]
    [Index(nameof(UserId), Name = "IX_InstallerDetails_UserId")]
    public partial class InstallerDetail
    {
        [Key]
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string InstallerAccreditationNumber { get; set; }
        public DateTime InstallerAccreditationExpiryDate { get; set; }
        public string DesignerLicenseNumber { get; set; }
        public DateTime DesignerLicenseExpiryDate { get; set; }
        public string ElectricianLicenseNumber { get; set; }
        public DateTime ElectricianLicenseExpiryDate { get; set; }
        public string DocInstaller { get; set; }
        public string DocDesigner { get; set; }
        public string DocElectrician { get; set; }
        [Column("OtherDocsCSV")]
        public string OtherDocsCsv { get; set; }
        public long UserId { get; set; }
        public string Address { get; set; }
        public bool? IsDesi { get; set; }
        public bool? IsElec { get; set; }
        public string IsGoogle { get; set; }
        public bool? IsInst { get; set; }
        public string PostCode { get; set; }
        public int? StateId { get; set; }
        public string StreetName { get; set; }
        public string StreetNo { get; set; }
        public string StreetType { get; set; }
        public string Suburb { get; set; }
        public string Unit { get; set; }
        public string UnitType { get; set; }
        [Column("latitude")]
        public string Latitude { get; set; }
        [Column("longitude")]
        public string Longitude { get; set; }
        public string Notes { get; set; }
        public string AreaName { get; set; }
        public int? SourceTypeId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.InstallerDetails))]
        public virtual AbpUser User { get; set; }
    }
}
