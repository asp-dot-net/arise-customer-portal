﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(EntityChangeId), Name = "IX_AbpEntityPropertyChanges_EntityChangeId")]
    public partial class AbpEntityPropertyChange
    {
        [Key]
        public long Id { get; set; }
        public long EntityChangeId { get; set; }
        [StringLength(512)]
        public string NewValue { get; set; }
        [StringLength(512)]
        public string OriginalValue { get; set; }
        [StringLength(96)]
        public string PropertyName { get; set; }
        [StringLength(192)]
        public string PropertyTypeFullName { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(EntityChangeId))]
        [InverseProperty(nameof(AbpEntityChange.AbpEntityPropertyChanges))]
        public virtual AbpEntityChange EntityChange { get; set; }
    }
}
