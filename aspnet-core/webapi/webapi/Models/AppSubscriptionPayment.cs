﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(EditionId), Name = "IX_AppSubscriptionPayments_EditionId")]
    [Index(nameof(ExternalPaymentId), nameof(Gateway), Name = "IX_AppSubscriptionPayments_ExternalPaymentId_Gateway")]
    [Index(nameof(Status), nameof(CreationTime), Name = "IX_AppSubscriptionPayments_Status_CreationTime")]
    public partial class AppSubscriptionPayment
    {
        [Key]
        public long Id { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int DayCount { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int EditionId { get; set; }
        public int Gateway { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public string SuccessUrl { get; set; }
        public int? PaymentPeriodType { get; set; }
        public int Status { get; set; }
        public int TenantId { get; set; }
        public string InvoiceNo { get; set; }
        public string Description { get; set; }
        public string ErrorUrl { get; set; }
        public string ExternalPaymentId { get; set; }
        [Required]
        public bool? IsRecurring { get; set; }
        public int EditionPaymentType { get; set; }

        [ForeignKey(nameof(EditionId))]
        [InverseProperty(nameof(AbpEdition.AppSubscriptionPayments))]
        public virtual AbpEdition Edition { get; set; }
    }
}
