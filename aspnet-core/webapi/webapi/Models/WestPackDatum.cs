﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class WestPackDatum
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public string ReceiptNumber { get; set; }
        public string Status { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
        public string SummaryCode { get; set; }
        public string TransactionType { get; set; }
        public string FraudGuardResult { get; set; }
        public DateTime TransactionTime { get; set; }
        public DateTime SettlementDate { get; set; }
        public string CustomerReferenceNumber { get; set; }
        public string PaymentReferenceNumber { get; set; }
        public string User { get; set; }
        public bool Voidable { get; set; }
        public bool Refundable { get; set; }
        public string Comment { get; set; }
        public string IpAddress { get; set; }
        public int? OrganaizationId { get; set; }
    }
}
