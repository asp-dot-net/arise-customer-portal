﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("Jobswarranty")]
    [Index(nameof(JobId), Name = "IX_Jobswarranty_JobId")]
    public partial class Jobswarranty
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int? JobId { get; set; }
        public int? ProductTypeId { get; set; }
        public string Filename { get; set; }
        public string Filepath { get; set; }
        public string FileType { get; set; }

        [ForeignKey(nameof(JobId))]
        [InverseProperty(nameof(JobType.Jobswarranties))]
        public virtual JobType Job { get; set; }
    }
}
