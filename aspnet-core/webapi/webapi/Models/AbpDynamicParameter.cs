﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class AbpDynamicParameter
    {
        public AbpDynamicParameter()
        {
            AbpDynamicParameterValues = new HashSet<AbpDynamicParameterValue>();
            AbpEntityDynamicParameters = new HashSet<AbpEntityDynamicParameter>();
        }

        [Key]
        public int Id { get; set; }
        public string ParameterName { get; set; }
        public string InputType { get; set; }
        public string Permission { get; set; }
        public int? TenantId { get; set; }

        [InverseProperty(nameof(AbpDynamicParameterValue.DynamicParameter))]
        public virtual ICollection<AbpDynamicParameterValue> AbpDynamicParameterValues { get; set; }
        [InverseProperty(nameof(AbpEntityDynamicParameter.DynamicParameter))]
        public virtual ICollection<AbpEntityDynamicParameter> AbpEntityDynamicParameters { get; set; }
    }
}
