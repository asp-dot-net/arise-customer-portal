﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(EntityDynamicParameterId), Name = "IX_AbpEntityDynamicParameterValues_EntityDynamicParameterId")]
    public partial class AbpEntityDynamicParameterValue
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Value { get; set; }
        public string EntityId { get; set; }
        public int EntityDynamicParameterId { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey(nameof(EntityDynamicParameterId))]
        [InverseProperty(nameof(AbpEntityDynamicParameter.AbpEntityDynamicParameterValues))]
        public virtual AbpEntityDynamicParameter EntityDynamicParameter { get; set; }
    }
}
