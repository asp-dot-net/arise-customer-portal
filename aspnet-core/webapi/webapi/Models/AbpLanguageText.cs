﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(Source), nameof(LanguageName), nameof(Key), Name = "IX_AbpLanguageTexts_TenantId_Source_LanguageName_Key")]
    public partial class AbpLanguageText
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        [Required]
        [StringLength(256)]
        public string Key { get; set; }
        [Required]
        [StringLength(128)]
        public string LanguageName { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        [Required]
        [StringLength(128)]
        public string Source { get; set; }
        public int? TenantId { get; set; }
        [Required]
        public string Value { get; set; }
    }
}
