﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(LeadId), Name = "IX_PromotionUsers_LeadId")]
    [Index(nameof(PromotionId), Name = "IX_PromotionUsers_PromotionId")]
    [Index(nameof(PromotionResponseStatusId), Name = "IX_PromotionUsers_PromotionResponseStatusId")]
    [Index(nameof(TenantId), Name = "IX_PromotionUsers_TenantId")]
    public partial class PromotionUser
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public DateTime ResponseDate { get; set; }
        public string ResponseMessage { get; set; }
        public int? PromotionId { get; set; }
        public int? LeadId { get; set; }
        public int? PromotionResponseStatusId { get; set; }

        [ForeignKey(nameof(LeadId))]
        [InverseProperty("PromotionUsers")]
        public virtual Lead Lead { get; set; }
        [ForeignKey(nameof(PromotionId))]
        [InverseProperty("PromotionUsers")]
        public virtual Promotion Promotion { get; set; }
        [ForeignKey(nameof(PromotionResponseStatusId))]
        [InverseProperty("PromotionUsers")]
        public virtual PromotionResponseStatus PromotionResponseStatus { get; set; }
    }
}
