﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), Name = "IX_AbpTenantNotifications_TenantId")]
    public partial class AbpTenantNotification
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Data { get; set; }
        [StringLength(512)]
        public string DataTypeName { get; set; }
        [StringLength(96)]
        public string EntityId { get; set; }
        [StringLength(512)]
        public string EntityTypeAssemblyQualifiedName { get; set; }
        [StringLength(250)]
        public string EntityTypeName { get; set; }
        [Required]
        [StringLength(96)]
        public string NotificationName { get; set; }
        public byte Severity { get; set; }
        public int? TenantId { get; set; }
    }
}
