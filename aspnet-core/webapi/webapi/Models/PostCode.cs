﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(StateId), Name = "IX_PostCodes_StateId")]
    public partial class PostCode
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(50)]
        public string PostalCode { get; set; }
        [Required]
        [StringLength(50)]
        public string Suburb { get; set; }
        [Column("POBoxes")]
        [StringLength(50)]
        public string Poboxes { get; set; }
        [StringLength(50)]
        public string Area { get; set; }
        public int StateId { get; set; }
        public string Areas { get; set; }

        [ForeignKey(nameof(StateId))]
        [InverseProperty("PostCodes")]
        public virtual State State { get; set; }
    }
}
