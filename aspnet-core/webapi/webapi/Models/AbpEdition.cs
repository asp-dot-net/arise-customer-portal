﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class AbpEdition
    {
        public AbpEdition()
        {
            AbpFeatures = new HashSet<AbpFeature>();
            AbpTenants = new HashSet<AbpTenant>();
            AppSubscriptionPayments = new HashSet<AppSubscriptionPayment>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(64)]
        public string DisplayName { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        [Required]
        [StringLength(32)]
        public string Name { get; set; }
        [Required]
        public string Discriminator { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? AnnualPrice { get; set; }
        public int? ExpiringEditionId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? MonthlyPrice { get; set; }
        public int? TrialDayCount { get; set; }
        public int? WaitingDayAfterExpire { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? DailyPrice { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? WeeklyPrice { get; set; }

        [InverseProperty(nameof(AbpFeature.Edition))]
        public virtual ICollection<AbpFeature> AbpFeatures { get; set; }
        [InverseProperty(nameof(AbpTenant.Edition))]
        public virtual ICollection<AbpTenant> AbpTenants { get; set; }
        [InverseProperty(nameof(AppSubscriptionPayment.Edition))]
        public virtual ICollection<AppSubscriptionPayment> AppSubscriptionPayments { get; set; }
    }
}
