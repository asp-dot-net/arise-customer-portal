﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(OrganizationUnitId), Name = "IX_AbpOrganizationUnitRoles_TenantId_OrganizationUnitId")]
    [Index(nameof(TenantId), nameof(RoleId), Name = "IX_AbpOrganizationUnitRoles_TenantId_RoleId")]
    public partial class AbpOrganizationUnitRole
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int? TenantId { get; set; }
        public int RoleId { get; set; }
        public long OrganizationUnitId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
