﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    public partial class RoofAngle
    {
        public RoofAngle()
        {
            Jobs = new HashSet<Job>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        [Required]
        [StringLength(200)]
        public string Name { get; set; }
        public int? DisplayOrder { get; set; }

        [InverseProperty(nameof(Job.RoofAngle))]
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
