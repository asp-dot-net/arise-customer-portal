﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenantId), nameof(OrganizationUnitId), Name = "IX_AbpUserOrganizationUnits_TenantId_OrganizationUnitId")]
    [Index(nameof(TenantId), nameof(UserId), Name = "IX_AbpUserOrganizationUnits_TenantId_UserId")]
    [Index(nameof(UserId), Name = "IX_AbpUserOrganizationUnits_UserId")]
    public partial class AbpUserOrganizationUnit
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long OrganizationUnitId { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        [Required]
        public bool? IsDeleted { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AbpUser.AbpUserOrganizationUnits))]
        public virtual AbpUser User { get; set; }
    }
}
