﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("STCPostalCodes")]
    public partial class StcpostalCode
    {
        [Key]
        public int Id { get; set; }
        public string PostCodeFrom { get; set; }
        public string PostCodeTo { get; set; }
        public int ZoneId { get; set; }
    }
}
