﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(TenancyName), nameof(UserNameOrEmailAddress), nameof(Result), Name = "IX_AbpUserLoginAttempts_TenancyName_UserNameOrEmailAddress_Result")]
    [Index(nameof(UserId), nameof(TenantId), Name = "IX_AbpUserLoginAttempts_UserId_TenantId")]
    public partial class AbpUserLoginAttempt
    {
        [Key]
        public long Id { get; set; }
        [StringLength(512)]
        public string BrowserInfo { get; set; }
        [StringLength(64)]
        public string ClientIpAddress { get; set; }
        [StringLength(128)]
        public string ClientName { get; set; }
        public DateTime CreationTime { get; set; }
        public byte Result { get; set; }
        [StringLength(64)]
        public string TenancyName { get; set; }
        public int? TenantId { get; set; }
        public long? UserId { get; set; }
        [StringLength(256)]
        public string UserNameOrEmailAddress { get; set; }
    }
}
