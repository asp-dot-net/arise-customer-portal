﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("LeadActivityLog")]
    [Index(nameof(LeadActionId), Name = "IX_LeadActivityLog_LeadActionId")]
    [Index(nameof(LeadId), Name = "IX_LeadActivityLog_LeadId")]
    public partial class LeadActivityLog
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
        public int LeadId { get; set; }
        public int ActionId { get; set; }
        public int? LeadActionId { get; set; }
        public string ActionNote { get; set; }
        public DateTime? ActivityDate { get; set; }
        public string ActivityNote { get; set; }
        public string Body { get; set; }
        public string MessageId { get; set; }
        public int? ReferanceId { get; set; }
        public bool? IsMark { get; set; }
        public int? TemplateId { get; set; }
        public string Subject { get; set; }
        public int? PromotionId { get; set; }
        public int? PromotionUserId { get; set; }
        public int? SectionId { get; set; }
        public string Todopriority { get; set; }
        public int? TodopriorityId { get; set; }
        public bool? IsTodoComplete { get; set; }
        public DateTime? TodoDueDate { get; set; }
        public string TodoResponse { get; set; }
        public string TodoTag { get; set; }
        public DateTime? TodoresponseTime { get; set; }

        [ForeignKey(nameof(LeadId))]
        [InverseProperty("LeadActivityLogs")]
        public virtual Lead Lead { get; set; }
        [ForeignKey(nameof(LeadActionId))]
        [InverseProperty("LeadActivityLogs")]
        public virtual LeadAction LeadAction { get; set; }
    }
}
