﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Table("Job", Schema = "HangFire")]
    public partial class Job1
    {
        public Job1()
        {
            JobParameters = new HashSet<JobParameter>();
            State1s = new HashSet<State1>();
        }

        [Key]
        public long Id { get; set; }
        public long? StateId { get; set; }
        [StringLength(20)]
        public string StateName { get; set; }
        [Required]
        public string InvocationData { get; set; }
        [Required]
        public string Arguments { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ExpireAt { get; set; }

        [InverseProperty(nameof(JobParameter.Job))]
        public virtual ICollection<JobParameter> JobParameters { get; set; }
        [InverseProperty(nameof(State1.Job))]
        public virtual ICollection<State1> State1s { get; set; }
    }
}
