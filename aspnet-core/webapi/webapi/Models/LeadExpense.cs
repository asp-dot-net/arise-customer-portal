﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace webapi.Models
{
    [Index(nameof(LeadSourceId), Name = "IX_LeadExpenses_LeadSourceId")]
    [Index(nameof(LeadinvestmentId), Name = "IX_LeadExpenses_LeadinvestmentId")]
    [Index(nameof(StateId), Name = "IX_LeadExpenses_StateId")]
    public partial class LeadExpense
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? TenantId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }
        public int? LeadSourceId { get; set; }
        public int? LeadinvestmentId { get; set; }
        public int? StateId { get; set; }
        [Column("dailyamount", TypeName = "decimal(18, 2)")]
        public decimal Dailyamount { get; set; }

        [ForeignKey(nameof(LeadSourceId))]
        [InverseProperty("LeadExpenses")]
        public virtual LeadSource LeadSource { get; set; }
        [ForeignKey(nameof(LeadinvestmentId))]
        [InverseProperty(nameof(LeadExpenseInvestment.LeadExpenses))]
        public virtual LeadExpenseInvestment Leadinvestment { get; set; }
        [ForeignKey(nameof(StateId))]
        [InverseProperty("LeadExpenses")]
        public virtual State State { get; set; }
    }
}
