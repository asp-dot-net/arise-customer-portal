import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AuthenticationModule} from './modules/authentication/authentication.module';
import {AuthenticationRoutingModule} from './modules/authentication/authentication-routing.module';
import {AsmodulesModule  } from './modules/asmodules/asmodules.module';
import {LayoutmoduleModule} from './modules/layoutmodule/layoutmodule.module';
//import { AuthenticationService } from './modules/Services/authentication.service';
 import { JwtModule } from "@auth0/angular-jwt";
import { environment } from '../environments/environment';
import { HttpModule } from './http.module';
export function tokenGetter() {
  return localStorage.getItem("token");
}
import { DatePipe } from '@angular/common';

import { NgHttpLoaderModule } from 'ng-http-loader';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    AuthenticationModule,
    LayoutmoduleModule,
    AsmodulesModule,
    HttpModule.forRoot({ environment }),
    FontAwesomeModule,

    JwtModule.forRoot({
      config: {
        //headerName: 'Authorization',       
        //tokenGetter: tokenGetter,       
        //authScheme: 'Bearer ',
        //allowedDomains: ["localhost:52027"],
        //disallowedRoutes: [],
        //throwNoTokenError: true
        tokenGetter
        //tokenGetter: () => {
        //  return localStorage.getItem('access_token');
        //},
        //allowedDomains: ["localhost"],
        //disallowedRoutes: []
      }
    }),
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
