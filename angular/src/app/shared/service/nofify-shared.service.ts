import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class NotifySharedService {
    notifySuccess(titel: string, msg: string){
        Swal.fire(titel, msg, 'success');
      }
      
      notifyError(titel: string, msg: string){
        Swal.fire(titel, msg, 'error');
      }
}
