import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserSharedService {

  readonly API_Url = environment.baseUrl;

  constructor(private http:HttpClient) { 
  }

  login(formData:any){
    return  this.http.post<any>(this.API_Url + '/auth/login', formData)
  }

  getUtility(): Observable<any>{
debugger
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.API_Url + '/utility', { headers })
  }
  
  getDashboardDetails(): Observable<any>{

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })
    
    return this.http.get<any>(this.API_Url + '/dashboard', { headers })
  }

  getOrderDetails(): Observable<any>{

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.API_Url + '/orderdetails', { headers })
  }

  getGridConnectionDetails(): Observable<any>{

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.API_Url + '/gridconnection', { headers })
  }

  getInstallationsDetails(): Observable<any>{

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.API_Url + '/installationdetails', { headers })
  }

  getBillingDetails(): Observable<any>{

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.API_Url + '/billing', { headers })
  }

  referFriend(formData:any){
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })
    return  this.http.post<any>(this.API_Url + '/referefriend', formData, { headers })
  }

  getreferFriend(): Observable<any>{

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.API_Url + '/referefriend', { headers })
  }

  forgotProjecNo(Email: any): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    })
    return this.http.post<any>(this.API_Url + '/forgotprojectno?email=' + Email, { headers });
  }

}
