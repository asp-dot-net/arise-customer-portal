import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationGuard } from './modules/authentication/authentication.guard';
import {AuthenticationModule} from './modules/authentication/authentication.module'; 
import {LayoutComponent} from './modules/layoutmodule/layout/layout.component';
import {DashboardComponent} from './modules/asmodules/dashboard/dashboard.component';
import {OrderDetailsComponent} from './modules/asmodules/order-details/order-details.component';
import {ContactUsComponent} from './modules/asmodules/contact-us/contact-us.component';
import {DocumentsComponent} from './modules/asmodules/documents/documents.component';
import {GridConnectionComponent} from './modules/asmodules/grid-connection/grid-connection.component';
import {InstallDetailsComponent} from './modules/asmodules/install-details/install-details.component';
import {DocWarrantyComponent} from './modules/asmodules/doc-warranty/doc-warranty.component';
import {BillingComponent} from './modules/asmodules/billing/billing.component';
import {ReferFriendComponent} from './modules/asmodules/refer-friend/refer-friend.component';
import {FeedbackComponent} from './modules/asmodules/feedback/feedback.component';

import {HeaderComponent} from './modules/layoutmodule/header/header.component';
import { FooterComponent} from './modules/layoutmodule/footer/footer.component';

const routes: Routes = [
  
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        canActivateChild: [AuthenticationGuard],
        children: [
          { path: 'dashboard', component: DashboardComponent },
          { path: 'orderdetails', component: OrderDetailsComponent },
          { path: 'contact', component: ContactUsComponent },
          { path: 'my-documents', component: DocumentsComponent },
          { path: 'grid-connection', component: GridConnectionComponent },
          { path: 'install-detail', component: InstallDetailsComponent },
          { path: 'doc-warranty', component: DocWarrantyComponent },
          { path: 'billing', component: BillingComponent },
          { path: 'refer-friend', component: ReferFriendComponent },
          { path: 'feedback', component: FeedbackComponent },
        ]
      }
      ]
  }

]
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
