import { InjectionToken } from '@angular/core';


export interface EnvironmentConfig {
  environment: {
    baseUrl: string;
    ImageDomainURL: string;
    DateFormat: string;
    // DomainUrl: string;
    // CompanyUrl: string;
    // CompanyName: string;
    // apiUrl: string,
    // //urlAddress: string;
    // abnurl: string;
    // googlemapurl: string;
    // Nationwide: {
    //   //Latitude: '-25.274399',
    //   //Longitude: '133.775131',
    //    Latitude:string,
    //    Longitude:string,
    // },
    // stripe_key:string;
    // smsurl: string;
    // FreeQuoteAllow:number;
    // LocationMapStaticURL:string;
  };
}

export const ENV_CONFIG = new InjectionToken<EnvironmentConfig>('EnvironmentConfig');
