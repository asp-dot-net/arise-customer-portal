import { Component } from '@angular/core';
import { CanActivate, Router, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, NavigationExtras } from '@angular/router';
import {
  // import as RouterEvent to avoid confusion with the DOM Event
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError, Event
} from '@angular/router'
// import { SharedService } from './shared.service';

import { AuthenticationService } from './modules/Services/authentication.service'; // './shared/services/authentication.service';
import {AuthenticationGuard} from './modules/authentication/authentication.guard';
import Swal from 'sweetalert2';
import { Spinkit } from 'ng-http-loader';
import { faAlignJustify } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'angular';
  namee: string = "";

  showLoadingIndicator = true;
  spinnerStyle = Spinkit;
  faAlignJustify = faAlignJustify;


  public isUserAuthenticated: boolean = false ;
  constructor( private _authService: AuthenticationService,  private router: Router, private http: HttpClient){ 
     this.router.events.subscribe((routerEvent: Event) => {
      // On NavigationStart, set showLoadingIndicator to ture
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
        //debugger;
      }

      // On NavigationEnd or NavigationError or NavigationCancel
      // set showLoadingIndicator to false
      if (routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationError ||
        routerEvent instanceof NavigationCancel) {
        this.showLoadingIndicator = false;
      }

    });
  }

    
  currentUrl: string = '';

    ngOnInit(): void{

    this.currentUrl = window.location.pathname;

    if(this.currentUrl == "/")
    {
      this.router.navigate(['/login']);
    }
    // if (this.currentUrl) {
    //   this.router.navigate(['/login']);
    // }
    // else {}
   
    this._authService.authChanged
      .subscribe(res => {
        this.isUserAuthenticated = res;
      });
    
    if (this._authService.isUserAuthenticated())
      this._authService.sendAuthStateChangeNotification(true);  
  }
  
}
