import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AuthenticationComponent } from './authentication/authentication.component';
import {LayoutmoduleModule} from './layoutmodule/layoutmodule.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AuthenticationComponent,
  ],
  imports: [
    CommonModule,
    LayoutmoduleModule
    , BrowserAnimationsModule
  ],
  providers:[DatePipe]
})
export class ModulesModule { }
