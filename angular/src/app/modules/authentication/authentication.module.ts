import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//import { ModuleWithProviders } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//import { AuthenticationComponent } from './authentication.component';
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {LoginComponent} from './login/login.component';

@NgModule({
  declarations: [
    //AuthenticationComponent,
    LoginComponent    
    //AutocompleteDirective
  ],
  imports: [
    FormsModule,
    HttpClientModule,    
    AuthenticationRoutingModule
  ],
  providers: [
    //{ provide: HTTP_INTERCEPTORS, useClass: LoadingScreenInterceptor, multi: true },
    //{ provide: ConfirmationDialogService, useValue: ConfirmationDialogService },
  ],
  bootstrap: [LoginComponent],
  //bootstrap: [AuthenticationModule],
  exports: [RouterModule]
})
export class AuthenticationModule { }
