import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { AuthenticationService } from '../../Services/authentication.service';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: []
})
export class LoginComponent implements OnInit {

  formModel = {
    Email: '',
    ProjectNo: ''
  }

  femail: any = '';

  constructor(private router: Router, private route: ActivatedRoute, private _service: UserSharedService, private _authService: AuthenticationService, 
   private _notify: NotifySharedService) {   }

  ngOnInit(): void {
  }

  onSubmit(form:NgForm) {
    return this._service.login(form.value).subscribe(
      (res:any)=>{
        localStorage.setItem('token', res.token);
        this._authService.sendAuthStateChangeNotification(res.isAuthSuccessful);
        if (res.isAuthSuccessful) {
          const redirectUrl = '/dashboard';

          const navigationExtras: NavigationExtras = {
            queryParamsHandling: 'preserve',
            preserveFragment: true
          }
          this.router.navigate([redirectUrl], navigationExtras);
        }
      },
      err => {
        if(err.status == 400 || err.status == 401)
          //this.msg = "Invalid Email and Project No.";
          this._notify.notifyError("Login Failed..", "Invalid Email and Project No.");
          else
          //console.log(err);
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }

  forgotProject(email: any)
  {
    console.log(email);
    // let e = JSON.stringify({'Email' : email});
    // let body = new HttpParams();
    // body = body.set('Email', e);
    
    return this._service.forgotProjecNo(email).subscribe(
      (res:any)=>{
        console.log(res);
        if(res.statusCode == 404){
          this._notify.notifyError("Not Found", res.message);
        }
        else if(res.statusCode == 200){
          this._notify.notifySuccess("Success", res.message);
          close();
        }
      },
      err => {
        this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )

  }

  close(){
    this.femail = '';
  }
}
