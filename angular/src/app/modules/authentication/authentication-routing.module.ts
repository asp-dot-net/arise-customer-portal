import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router'



import {LoginComponent} from './login/login.component';

const authenticationRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  // { path: 'settings', component: AccountDetailsComponent }

]

@NgModule({
  declarations: [],
  imports: [    
    RouterModule.forChild(authenticationRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthenticationRoutingModule { }
