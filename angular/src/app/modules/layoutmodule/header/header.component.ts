import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../Services/authentication.service';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private _authService: AuthenticationService, private _userSharedService: UserSharedService, private _notify: NotifySharedService) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') != null)
    this.getUtility();
  }

  displayName: any = 'salesdrive';
  telPhone: any = null;
  logo: any = "assets/images/logo_solarblue.svg";

  getUtility(){
    return this._userSharedService.getUtility().subscribe(
      (res:any)=>{
        this.displayName = res.organizationName;
        this.telPhone = res.telPhone;
        if(res.logo != null){
          this.logo = res.logo;
        }
      },
      err => {
          // console.log(err);
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }

  logout(): void
  {
    this._authService.logout();
  }
}
