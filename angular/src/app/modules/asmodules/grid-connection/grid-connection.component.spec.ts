import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridConnectionComponent } from './grid-connection.component';

describe('GridConnectionComponent', () => {
  let component: GridConnectionComponent;
  let fixture: ComponentFixture<GridConnectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridConnectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
