import { Component, OnInit } from '@angular/core';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-grid-connection',
  templateUrl: './grid-connection.component.html',
  styleUrls: ['./grid-connection.component.css']
})
export class GridConnectionComponent implements OnInit {

  _dateFormat:string = environment.DateFormat;

  meterApplicationDate: any;
  meterApplicationApprovedDate: any;
  nmiNumber: string = '';
  energyProvider: string = '';
  flag1: any;
  flag2: any;
  imgLink: string = '';
  imgLink1: string = '';

  constructor(private _userSharedService: UserSharedService, private _notify: NotifySharedService, private _datePipe: DatePipe) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails(){
    return this._userSharedService.getGridConnectionDetails().subscribe(
      (res:any)=>{
        this.meterApplicationDate = res.meterApplicationDate == null || res.meterApplicationDate == '' ? 'Under process' : this._datePipe.transform(res.meterApplicationDate, this._dateFormat);
        this.imgLink = this.meterApplicationDate == "Under process" ? "assets/images/icon-cal1.png" : "assets/images/icon-cal.png"
        this.meterApplicationApprovedDate = res.meterApplicationApprovedDate == null || res.meterApplicationApprovedDate == '' ? 'Under process' : this._datePipe.transform(res.meterApplicationApprovedDate, this._dateFormat);
        this.imgLink1 = this.meterApplicationApprovedDate == "Under process" ? "assets/images/icon-cal1.png" : "assets/images/icon-cal.png"
        this.flag1 = this.meterApplicationDate == 'Under process' || this.meterApplicationApprovedDate == 'Under process' ? false : true;

        this.nmiNumber = res.nmiNumber;
        this.energyProvider = res.energyProvider;
        this.flag2 = res.nmiNumber == '' || res.energyProvider == '' ? false : true;
      },
      err => {
          // console.log(err);
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }
}
