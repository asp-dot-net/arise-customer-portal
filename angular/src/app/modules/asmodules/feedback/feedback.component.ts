import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  stars: number[] = [1, 2, 3, 4, 5];
  stars1: number[] = [1, 2, 3, 4, 5];
  stars2: number[] = [1, 2, 3, 4, 5];
  stars3: number[] = [1, 2, 3, 4, 5];
  
  selectedValue: any;
  isMouseover = true;

  constructor() { }

  ngOnInit(): void {
  }

  countStar(star: any) {
    this.isMouseover = false;
    this.selectedValue = star;
    console.log('Value of star', star);
  }

  addClass(star: number) {
    if (this.isMouseover) {
      this.selectedValue = star;
    }
  }
  
  removeClass() {
     if (this.isMouseover) {
        this.selectedValue = 0;
     }
  }

}
