import { Component, OnInit } from '@angular/core';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-install-details',
  templateUrl: './install-details.component.html',
  styleUrls: ['./install-details.component.css']
})
export class InstallDetailsComponent implements OnInit {

  _dateFormat:string = environment.DateFormat;

  paymentDue: any;
  installationBookingDate: any;
  installationStatus: any;
  netMeterStatus: string = '';

  imgLink: any;
  flag1: any;

  constructor(private _userSharedService: UserSharedService, private _notify: NotifySharedService, private _datePipe: DatePipe) { }

  ngOnInit(): void {  
    this.getDetails();
  }

  getDetails(){
    return this._userSharedService.getInstallationsDetails().subscribe(
      (res:any)=>{
        this.paymentDue = res.paymentDue;
        this.installationBookingDate = res.installationBookingDate == null || res.installationBookingDate == '' ? 'Under process' : this._datePipe.transform(res.installationBookingDate, this._dateFormat);
        this.imgLink = this.installationBookingDate == "Under process" ? "assets/images/icon-cal1.png" : "assets/images/icon-cal.png"
        
        this.installationStatus = res.installationStatus == null || res.installationStatus == '' ? 'Under process' : 'Installation Completed';
        this.netMeterStatus = res.netMeterStatus == '' ? 'Under process' : 'Completed';

        this.flag1 = this.installationBookingDate == 'Under process' || this.installationStatus == 'Under process' ? false : true;
      },
      err => {
          // console.log(err);
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }
}
