import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallDetailsComponent } from './install-details.component';

describe('InstallDetailsComponent', () => {
  let component: InstallDetailsComponent;
  let fixture: ComponentFixture<InstallDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstallDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
