import { Component, OnInit } from '@angular/core';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit {
  
  projectNo: any;
  customerName: string = '';
  paymentDue: any;
  invoicePdf: any;
  invoiceDetails: any = [];
  sr: number = 1;
  url: string = environment.ImageDomainURL;

  constructor(private _userSharedService: UserSharedService, private _notify: NotifySharedService) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails(){
    return this._userSharedService.getBillingDetails().subscribe(
      (res:any)=>{
        this.projectNo = res.projectNo;
        this.customerName = res.customerName;
        this.paymentDue = res.paymentDue;
        this.invoicePdf = res.invoicePdf;
        this.invoiceDetails = res.invoices;
      },
      err => {
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }
}
