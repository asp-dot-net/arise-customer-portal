import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { RouterModule } from '@angular/router';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { DocumentsComponent } from './documents/documents.component';
import { GridConnectionComponent } from './grid-connection/grid-connection.component';
import { InstallDetailsComponent } from './install-details/install-details.component';
import { DocWarrantyComponent } from './doc-warranty/doc-warranty.component';
import { BillingComponent } from './billing/billing.component';
import { ReferFriendComponent } from './refer-friend/refer-friend.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DashboardComponent,
    OrderDetailsComponent,
    ContactUsComponent,
    DocumentsComponent,
    GridConnectionComponent,
    InstallDetailsComponent,
    DocWarrantyComponent,
    BillingComponent,
    ReferFriendComponent,
    FeedbackComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule, ReactiveFormsModule
  ],
  providers:[DatePipe]
})
export class AsmodulesModule { }
