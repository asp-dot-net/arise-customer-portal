import { Component, OnInit } from '@angular/core';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { environment } from 'src/environments/environment';
import {map} from 'rxjs/operators';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  quoteLink: any;
  projectNo: any;
  roofType: any;
  roofAngle: any;
  buildingType: any;
  meterPhase: any;
  nmiNumber: any;
  installationType: any;
  paymentDue: any;
  productItems: any = [];

  //productItems:any;
  constructor(private _userSharedService: UserSharedService, private _notify: NotifySharedService) { }

  ngOnInit(): void {  
    this.getDetails();
  }

  getDetails(){
    return this._userSharedService.getOrderDetails().subscribe(
      (res:any)=>{
        this.quoteLink = environment.ImageDomainURL + res.quoteLink;
        this.projectNo = res.projectNo,
        this.roofType = res.roofType;
        this.roofAngle = res.roofAngle;
        this.buildingType = res.buildingType;
        this.meterPhase = res.meterPhase;
        this.nmiNumber = res.nmiNumber;
        this.installationType = res.installationType;
        this.paymentDue = res.paymentDue;
        this.productItems = res.productItems;
      },
      err => {
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }

}