import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { AuthenticationService } from '../../Services/authentication.service';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';

@Component({
  selector: 'app-refer-friend',
  templateUrl: './refer-friend.component.html',
  styleUrls: ['./refer-friend.component.css'],
  animations: []
})
export class ReferFriendComponent implements OnInit {
 @ViewChild('closerefer') closerefer: any;

  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  formModel = {
    FirstName: '',
    LastName: '',
    Email: '',
    Mobile: '',
    PropertyAddress: '',
    agree: false,
  }
  constructor(private _service: UserSharedService, private _notify: NotifySharedService) { }

  ngOnInit(): void {
    this.GetReferFriend();
  }

  List: any = [];
  sum: number = 0;

  GetReferFriend(){
    return this._service.getreferFriend().subscribe(
      (res:any)=>{
        this.List = res;
        this.sum = this.List.reduce((sum: number, b: any) => b.numberOfHours, 0);
      },
      err => {
        this._notify.notifyError("Oops...", "Something went wrong!")
    }
    )
  }

  onSubmit(form:NgForm) {
    return this._service.referFriend(form.value).subscribe(
      (res:any)=>{
        this.closerefer.nativeElement.click();
        this.cleanObject(this.formModel);
        this.GetReferFriend();
        this._notify.notifySuccess("Success.", "Saved Successfully..");
      },
      err => {
        this._notify.notifyError("Oops...", "Something went wrong!");
        // if(err.status == 400 || err.status == 401)
        // this._notify.notifyError("Oops...", "Something went wrong!");
        //   else
        //   //console.log(err);
        //   this._notify.notifyError("Oops...", "Something went wrong!");
      }
    )
  }

  cleanObject(o : any) {
    for (let [key, value] of Object.entries(o)) {
      let propType = typeof(o[key]);
  
      switch (propType) {
        case "number" :
          o[key] = 0;
          break;
  
        case "string":
          o[key] = '';
          break;
  
        case "boolean":
          o[key] = false;    
          break;
  
        case "undefined":
          o[key] = undefined;   
          break;
  
        default:
          if(value === null) {
              continue;
          }
  
          this.cleanObject(o[key]);
          break;
      }
    }
  }
}
