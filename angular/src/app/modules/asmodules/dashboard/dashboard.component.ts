import { Component, OnInit } from '@angular/core';
import { CanActivate, Router, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, NavigationExtras } from '@angular/router';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { environment } from 'src/environments/environment';
import { NotifySharedService } from 'src/app/shared/service/nofify-shared.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router, private _userSharedService: UserSharedService, private _notify: NotifySharedService) { }

  customerName : string = '';
  projectNo: string = '';
  systemKw: string = '';
  orgName: string = '';
  quoteLink: any = null;
  orderDettail: any;
  gridConnApp: any;
  installationDetails: any;

  ngOnInit(): void {
    // console.log()
    this.getDetails();
  }


  getDetails(){
   
    return this._userSharedService.getDashboardDetails().subscribe(
      (res:any)=>{
        this.customerName = res.customerName;
        this.projectNo = res.projectNo;
        this.systemKw = res.systemKw;
        this.orgName = res.organizationName;
        this.quoteLink = environment.ImageDomainURL + res.quoteLink;
        this.orderDettail = res.orderDettail;
        this.gridConnApp = res.gridConnApp;
        this.installationDetails = res.installationDetails;
      },
      err => {
          // console.log(err);
          this._notify.notifyError("Oops...", "Something went wrong!")
      }
    )
  }
}
