import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, of, Observable, throwError, Subject, } from 'rxjs';
import { map, catchError, delay, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
//Config
//import { AppConfig, APP_CONFIG } from '../app-config.module';
import { ENV_CONFIG, EnvironmentConfig } from '../environment-config';
//import { environment } from '../../environments/environment';
//import { environment } from '../../environments/environment.prod';
@Injectable({
  providedIn: 'root'
})
export class GeneralserviceService {
  public apiUrl: string;
  public abnUrl: string;
  public smsUrl: string;
  public googlemapUrl: string;
  // @Inject(APP_CONFIG) private config: AppConfig
  constructor(@Inject(ENV_CONFIG) private config: EnvironmentConfig,
    private _http: HttpClient,private _route: Router) {
    this.apiUrl = `${config.environment.baseUrl}`;
    this.abnUrl = `${config.environment.abnurl}`;
    this.googlemapUrl = `${config.environment.googlemapurl}`;
    this.smsUrl = `${config.environment.smsurl}`;
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  options_: any = {
    //body: content_,
    observe: "response",
    //responseType: "text",
    headers: new HttpHeaders({
      "Content-Type": "multipart/form-data",
      "Accept": "application/json",
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': "DELETE, POST, GET, OPTIONS",
      'Access-Control-Allow-Headers': "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
    })
  };
  CreateAuthorizationHeader() {
    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    return headers;
  }
  getLoggedinUserDetail = () => {
    var userData = localStorage.getItem('currentUser');
    if (userData !== null && userData !== undefined && userData !== "") {
      return JSON.parse(userData);
    }
    else {
      var location = window.location.toString().toLowerCase();
      if (location.includes('sendmail')) {
        this._route.navigate([location['href']]);
      }
      else {
        this._route.navigate(['/login']);
      }
    }
  }

  getUserId = () => {
    let userDetail = this.getLoggedinUserDetail();
    if (userDetail.isSuperadmin) {
      let id = localStorage.getItem('SuperAdminUserId');
      if (id == null || id == '') {
        //this.toastr.error('Please Select User');
      }
      else {
        return Number(id);
      }
    }
    else {
      return userDetail.id;
    }
  }
  public navigateToDefaultRoute() {
    // if (this.role.isAdmin || this.role.isShowDashBoard) {
    //   this._route.navigate(['/dashboard']);
    // }
    // else {
    //   this._route.navigate(['/discussionwall']);
    // }
  }
  /** POST: add a new method */
  postSaveservice(CommonMst: any, apiName: any): Observable<any> {
    try {
      return this._http.post<any>(`${this.apiUrl}` + apiName, CommonMst, { headers: this.CreateAuthorizationHeader() })
        .pipe(map(user => {
          return {
            data: user,
          };
        }, catchError(this.handleError)));
    } catch (e) {
      //this.logger.log(e)
    }
  }
  // Calls the controller method
  public broadcastMessage(CommonMst: any, apiName: any) {
    //this._http.post(this.POST_URL, msgDto).subscribe(data => console.log(data));
    // this.connection.invoke("SendMessage1", msgDto.user, msgDto.msgText).catch(err => console.error(err));    // This can invoke the server method named as "SendMethod1" directly.
  }
  message: string;
  response: any;
  async fetchData(apiName: any) {
    this.message = "Fetching..";
    this.response = "";
    this.response = await this._http
      .get<any>(`${this.apiUrl}` + apiName)
      .pipe(delay(100))
      .toPromise();
    //this.message = "Fetched";
    return this.response;
  }
  getRecordservice(apiName: any): Observable<any> {    
    try {
      return this._http.get<any>(`${this.apiUrl}` + apiName, { headers: this.CreateAuthorizationHeader() })
        .pipe(map(response => {
          return {
            data: response,
          };
        }, catchError(this.handleError) //error => console.log(this.handleError)
        ));
    } catch (e) {
      //this.logger.log(e)
    }
  }

  getABNdata(apiName: any) {
    try {
      return this._http.get(`${this.abnUrl}`.replace("abnnumbereplace", apiName), { responseType: 'text' })
        .pipe(map(user => {
          return {
            data: user,
          };
        }, catchError(this.handleError)));
    } catch (e) {
      //this.logger.log(e)
    }
  }
  //send SMS api
  SendSMS(apiName: any) {
    try {
      return this._http.get(`${this.smsUrl}`.replace("abnnumbereplace", apiName), { responseType: 'text' })
        .pipe(map(user => {
          return {
            data: user,
          };
        }, catchError(this.handleError)));
    } catch (e) {
      //this.logger.log(e)
    }
  }
  //Get SMS api
  GetSMS(apiName: any) {
    try {
      return this._http.get(`${this.smsUrl}`.replace("abnnumbereplace", apiName), { responseType: 'text' })
        .pipe(map(user => {
          return {
            data: user,
          };
        }, catchError(this.handleError)));
    } catch (e) {
      //this.logger.log(e)
    }
  }
  getLocationLatLong(apiName: any) {
    try {
      return this._http.get(`${this.googlemapUrl}`.replace("addresslocation", apiName), { responseType: 'text' })
        .pipe(map(user => {
          return {
            data: user,
          };
        }, catchError(this.handleError)));
    } catch (e) {
      //this.logger.log(e)
    }
  }
  public updateRecordservice(apiName: any) {
    //return this._http.put(`${this.apiurl}/customers/${customer.id}`, customer);
    try {
      let promise = new Promise((resolve, reject) => {
        this._http.get(`${this.apiUrl}` + apiName)
          .toPromise()
          .then(
            res => {
              resolve(res);
            }, catchError(this.handleError));
      })
      return promise; 47
    } catch (e) {
      //this.logger.log(e)
    }
  }

  public deleteRecordservice(apiName: any, id: string) {
    return this._http.delete(`${this.apiUrl}` + apiName + `/${id}`);
  }
  //Upload File


  postFile(fileToUpload: File, apiName: any): Observable<any> {
    try {
      const formData: FormData = new FormData();
      formData.append('file', fileToUpload, fileToUpload.name);
      return this._http.post<any>(`${this.apiUrl}` + apiName, formData)
        .pipe(map(user => {
          return {
            data: user,
          };
        }, catchError(this.handleError)));
    } catch (e) {
      //this.logger.log(e)
    }
  }
  public download(fileUrl: string) : Observable<any>{
    return this._http.get(`${this.apiUrl}/${fileUrl}`, {
      reportProgress: true,
      responseType: 'blob',
    });
  }
  //DownloadFile
  downloadPDF(url): Observable<any> {
    return this._http.get(`${this.apiUrl}` + url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/pdf',  //text/csv
      }), responseType: 'blob'
    }).pipe(
      map(
        (res) => {
          //return { res: res }
          return new Blob([res], { type: 'application/pdf' })
        })
    );
  }
  /**
       * Method is use to download file.
       * @param data - Array Buffer data
       * @param type - type of the document.
       */
  downLoadFile(data: any, type: string) {
    let blob = new Blob([data], { type: type });
    let url = window.URL.createObjectURL(blob);
    let pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
      alert('Please disable your Pop-up blocker and try again.');
    }
  }
  //Error Hendler
  handleError(error: any) {
    console.error(error);
    // let errorMessage = '';

    // if (error.error instanceof ErrorEvent) {

    //   // client-side error

    //   errorMessage = Error: ${ error.error.message };

    // } else {

    //   // server-side error

    //   errorMessage = Error Code: ${ error.status } \nMessage: ${ error.message };

    // }

    return throwError(error || 'Server error');
    //return Observable.throw(error.json().error || 'Server error');
  }

  
}
