import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements HttpInterceptor {

  constructor(private _router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
            this.handleError(error);

          } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            this.handleError(error);
          }
          //window.alert(errorMessage);
          return throwError(error.error.errors);
        })
      );
    //return next.handle(req)
    //  .pipe(
    //    catchError((error: HttpErrorResponse) => {
    //      debugger
    //      let errorMessage = this.handleError(error);
    //      return throwError(errorMessage);
    //    })
    //  )
  }
  private handleError = (error: HttpErrorResponse): string => {
    debugger
    if (error.status === 404) {
      return this.handleNotFound(error);
    }
    else if (error.status === 400) {
      return this.handleBadRequest(error);
    }
    else if (error.status === 401) {
      return this.handleUnauthorized(error);
    }
    else if (error.status === 403) {
      return this.handleForbidden(error);
    }
  }
  private handleForbidden = (error: HttpErrorResponse) => {
    this._router.navigate(["/forbidden"], { queryParams: { returnUrl: this._router.url } });
    return "Forbidden";
  }
  private handleUnauthorized = (error: HttpErrorResponse) => {
    //if (this._router.url === '/authentication/login') {
    //  return 'Authentication failed. Wrong Username or Password';
    //}
    if (this._router.url.startsWith('/login')) {
      return error.error.error;
    }
    else {
      //this._router.navigate(['/authentication/login']);
      this._router.navigate(['/login'], { queryParams: { returnUrl: this._router.url } });
      return error.error.error;
    }
  }
  private handleNotFound = (error: HttpErrorResponse): string => {
    this._router.navigate(['/404']);
    return error.message;
  }

  private handleBadRequest = (error: HttpErrorResponse): string => {
    //if (this._router.url === '/register' || this._router.url.startsWith('/authentication/resetpassword')) {
    if (this._router.url != '') {
    let message = '';
      const values = Object.values(error.error.errors);
      values.map((m: string) => {
        message += m + '<br>';
      })

      return message.slice(0, -4);
    }
    else {
      return error.error ? error.error : error.error.errors;
      //return error.error ? error.error : error.message;
    }
  }
}
