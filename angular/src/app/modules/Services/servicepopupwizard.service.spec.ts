import { TestBed } from '@angular/core/testing';

import { ServicepopupwizardService } from './servicepopupwizard.service';

describe('ServicepopupwizardService', () => {
  let service: ServicepopupwizardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicepopupwizardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
