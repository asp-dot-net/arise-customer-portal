import { EventEmitter, Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Message } from '../_interfaces/response/message';
//import { environment } from '../../environments/environment';
import { environment } from '../../environments/environment.prod';
import * as signalR from '@microsoft/signalr';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ChatService {

  //private _hubConnection: HubConnection | undefined;
  private _hubConnection: signalR.HubConnection
  messageReceived = new EventEmitter<Message>();
  connectionEstablished = new EventEmitter<Boolean>();
  public connectionId : string;
  private connectionIsEstablished = false;
  private  connection: any = new signalR.HubConnectionBuilder().withUrl("http://localhost:52027/message-hub")   // mapping to the chathub as in startup.cs
                                         .configureLogging(signalR.LogLevel.Information)
                                         .build();
   //readonly POST_URL = "https://localhost:44379/api/chat/send"

  public messgemodel : MessageDto = new MessageDto(); 
  constructor(private http: HttpClient) {
     this.connection.onclose(async () => {
       await this.start();
     });

    //this.ReceivedQuoteResponseMsg();
    this.connection.on("ReceiveOne", (message) => { this.mapReceivedMessage(message); });
    this.start();  
    //this.createConnection();
    //this.registerOnServerEvents();
    //this.startConnection();
  }
  
  // Strart the connection
  public async start() {
    try {
      await this.connection.start();
      console.log("connected");
    } catch (err) {
      console.log(err);
      setTimeout(() => this.start(), 5000);
    } 
  }
  public ReceivedQuoteResponseMsg(){
    
    this.connection.on("ReceiveResponseMsg", (data: any) => {  
          
      if(data != null || data != '') 
      {
        console.log(data)
      }
    });
  }

  private subject = new Subject<any>();

  private receivedMessageObject: MessageDto = new MessageDto();
  private sharedObj = new Subject<MessageDto>();

  private mapReceivedMessage(messgemodel:MessageDto): void {
    debugger
    this.receivedMessageObject.iQuoteRequestMessage_Id = messgemodel.iQuoteRequestMessage_Id;
    this.receivedMessageObject.iQuoteRequestMessage_RequestContactId = messgemodel.iQuoteRequestMessage_RequestContactId;
    this.receivedMessageObject.vQuoteRequestMessage_Message = messgemodel.vQuoteRequestMessage_Message;
    this.receivedMessageObject.cQuoteRequestMessage_MsgFlag = messgemodel.cQuoteRequestMessage_MsgFlag;
    this.receivedMessageObject.cQuoteRequestMessage_buyerMsgReadFlag = messgemodel.cQuoteRequestMessage_buyerMsgReadFlag;
    this.receivedMessageObject.cQuoteRequestMessage_sellerMsgReadFlag = messgemodel.cQuoteRequestMessage_sellerMsgReadFlag;
    this.receivedMessageObject.cQuoteRequestMessage_ActiveFlag = messgemodel.cQuoteRequestMessage_ActiveFlag;
    this.receivedMessageObject.cQuoteRequestMessage_ReplicaFlag = messgemodel.cQuoteRequestMessage_ReplicaFlag;
    this.receivedMessageObject.vQuoteRequestMessage_MsgType = messgemodel.vQuoteRequestMessage_MsgType;    
    this.sharedObj.next(this.receivedMessageObject);
 }

  /* ****************************** Public Mehods **************************************** */
  readonly POST_URL ='http://localhost:52027/api/chatAPI/send';
  // Calls the controller method
  public broadcastMessage(msgDto: any) {    
    this.http.post(this.POST_URL, msgDto).subscribe(data => console.log(data));
    //this.connection.invoke("SendMessage1", msgDto.user, msgDto.msgText).catch(err => console.error(err));    // This can invoke the server method named as "SendMethod1" directly.
  }

  public retrieveMappedObject(): Observable<MessageDto> {
    debugger
    return this.sharedObj.asObservable();
  }

  sendMessage(message: Message) {    
    this._hubConnection.invoke('MessageReceived', message);
  }

  // private createConnection() {
  //   debugger
  //   //  this._hubConnection =  new signalR.HubConnectionBuilder()  
  //   //    //.withUrl(window.location.href + 'MessageHub')  
  //   //    .withUrl(`http://localhost:52027/` + 'message-hub')
  //   //    .build();
  //   //    this._hubConnection
  //   //    .start()
  //   //    .then(() => console.log('Connection started'))
  //   //    .then(() => this.getConnectionId())
  //   //    .catch(err => console.log('Error while starting connection: ' + err))


  //    this._hubConnection = new signalR.HubConnectionBuilder()    
  //    .withUrl('http://localhost:52027/message-hub')
  //    //.configureLogging(signalR.LogLevel.Information)
  //    .build();
   

  //  this._hubConnection.start().catch(err => console.error(err.toString()));

  //  this._hubConnection.on('ReceiveResponseMsg', (data: any) => {
  //   debugger
  //    if(data != null || data != '') 
  //    {
  //      console.log(data)
  //    }
   
  // //   //const received = `Received: ${data}`;
  // //   //this.messages.push(received);
  //  });
  //   // this._hubConnection = new HubConnectionBuilder()
  //   //   //.withUrl(window.location.href + 'MessageHub')  
  //   //   .withUrl(`http://localhost:52027/` + 'MessageHub')
  //   //   .build();
  //   //   this._hubConnection
  //   //   .start()
  //   //   .then(() => console.log('Connection started'))
  //   //   .then(() => this.getConnectionId())
  //   //   .catch(err => console.log('Error while starting connection: ' + err))
  // }
  public getConnectionId = () => {
    
    this._hubConnection.invoke('getconnectionid').then(
      (data) => {
        console.log(data);
          this.connectionId = data;
        }
    ); 
  }
  //  private startConnection(): void {
  //    debugger
  //    this._hubConnection
  //      .start()
  //      .then(() => {
  //        this.connectionIsEstablished = true;
  //        console.log('Hub connection started');
  //        this.connectionEstablished.emit(true);
  //      })
  //      .catch(err => {
  //        console.log('Error while establishing connection, retrying...');
  //        //setTimeout(function () { this.startConnection(); }, 5000);  
  //      });
  //  }

  // private registerOnServerEvents(): void {
  //   this._hubConnection.on('MessageReceived', (data: any) => {
  //     this.messageReceived.emit(data);
  //   });
  // }
}
export class MessageDto {
  public iQuoteRequestMessage_Id :string = '';
  public iQuoteRequestMessage_RequestContactId:string = '';
  public vQuoteRequestMessage_Message : string = '';
  public cQuoteRequestMessage_MsgFlag:string='';
  public cQuoteRequestMessage_buyerMsgReadFlag:string='';
 public cQuoteRequestMessage_sellerMsgReadFlag:string = '';
 public cQuoteRequestMessage_ActiveFlag :string ='';
 public cQuoteRequestMessage_ReplicaFlag:string = '';
 public dQuoteRequestMessage_CreatedOn:Date = new Date();
 public dQuoteRequestMessage_ModifyOn:Date = new Date();
 public iQuoteRequestMessage_CreatedBy:string = '';
 public iQuoteRequestMessage_ModifyBy:string = '';
 public vQuoteRequestMessage_MsgType:string = '';
 
}