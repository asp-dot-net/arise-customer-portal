import { InjectionToken } from '@angular/core';
import { NgWizardConfigs } from '../utils/interfaces';


export const NG_WIZARD_CONFIG_TOKEN = new InjectionToken<NgWizardConfigs>('ngWizardCustom.config');
