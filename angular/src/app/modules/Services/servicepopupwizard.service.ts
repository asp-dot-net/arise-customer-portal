import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {MultiStepFormComponent} from '../../app/modules/layoutmodule/service-popup-wizard/service-popup-wizard.component';
@Injectable({
  providedIn: 'root'
})
export class ServicepopupwizardService {

  constructor(private modalService: NgbModal) { }

  public confirm(
    selectCategoryId:string,
    selectPostcodeId:string,
    selectimagepath:string,
    selectcategorytitle:string,
    selectcategorydesc:string,
    title: string,
    message: string,
    btnOkText: string = 'OK',
    btnCancelText: string = 'Cancel',
    dialogSize: 'sm'|'lg' = 'lg'): Promise<boolean> {
    const modalRef = this.modalService.open(MultiStepFormComponent, { size: dialogSize });
    modalRef.componentInstance.selectCategoryId = selectCategoryId;
    modalRef.componentInstance.selectPostcodeId = selectPostcodeId;
    modalRef.componentInstance.selectimagepath = selectimagepath;
    modalRef.componentInstance.selectcategorytitle = selectcategorytitle;
    modalRef.componentInstance.selectcategorydesc = selectcategorydesc;
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }
}
