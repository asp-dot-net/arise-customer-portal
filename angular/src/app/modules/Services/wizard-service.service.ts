import { Injectable, Type } from '@angular/core';
import { isObservable, Observable } from 'rxjs';
import { NgWizardConfig, StepValidationArgs, STEP_STATE, THEME } from 'ng-wizard';

import { NgWizardDataService } from './ng-wizard-data.service';
import { StepChangedArgs } from '../utils/interfaces';


@Injectable({
  providedIn: 'root'
})
export class WizardServiceService {

  constructor(private ngWizardDataService: NgWizardDataService) { }
  
  reset() {
    this.ngWizardDataService.resetWizard();
  }

  next() {
    this.ngWizardDataService.showNextStep();
  }

  previous() {
    this.ngWizardDataService.showPreviousStep();
  }

  show(index: number) {
    this.ngWizardDataService.showStep(index);
  }

  theme(theme: THEME) {
    this.ngWizardDataService.setTheme(theme);
  }

  stepChanged(): Observable<StepChangedArgs> {
    return this.ngWizardDataService.stepChangedArgs$;
  }
  
  config: NgWizardConfig = {
    selected: 0,
    //theme: THEME.arrows,
    
    toolbarSettings: {
      showNextButton: false, showPreviousButton: false,
      // toolbarExtraButtons: [
      //   {
      //     text: 'Finish',
      //     class: 'btn btn-info',
      //     event: () => alert('Finished!!!')
      //   },
      // ]
    }
  };
  stepDefinitions: StepDefinition[] = [
    {     
      canEnter: this.validateStep.bind(this, 'entry'),
      canExit: this.validateStep.bind(this, 'exit'),
    },
     
  ];
  private validateStep(type: string, args: StepValidationArgs) {
    //debugger
    let step = type == 'entry' ? args.toStep : args.fromStep;
    let stepSpecificValidateMethod;

    if (step && step.componentRef) {
      stepSpecificValidateMethod = type == 'entry' ? step.componentRef.instance.validateEntryToStep : step.componentRef.instance.validateExitFromStep;
    }

    if (stepSpecificValidateMethod) {
      if (typeof stepSpecificValidateMethod === typeof true) {
        return <boolean>stepSpecificValidateMethod;
      }
      else if (stepSpecificValidateMethod instanceof Function) {
        stepSpecificValidateMethod = stepSpecificValidateMethod.bind(step.componentRef.instance);
        let result = stepSpecificValidateMethod();

        if (isObservable<boolean>(result)) {
          return result;
        }
        else if (typeof result === typeof true) {
          return <boolean>result;
        }
      }
    }

    return true;
  }
}
export interface StepDefinition {
  // title: string;
  // description: string;
  // state?: STEP_STATE;
  // component: Type<any>;
  canEnter?: boolean | ((args: StepValidationArgs) => boolean) | ((args: StepValidationArgs) => Observable<boolean>);
  canExit?: boolean | ((args: StepValidationArgs) => boolean) | ((args: StepValidationArgs) => Observable<boolean>);
}