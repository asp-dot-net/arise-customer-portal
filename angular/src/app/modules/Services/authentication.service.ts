import { HttpClient, HttpParams  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { ENV_CONFIG, EnvironmentConfig } from '../../environment-config';
import { Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { tap, delay } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { UserSharedService } from 'src/app/shared/service/user-shared.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public apiUrl: string;
  isLoggedIn = false;
  private _authChangeSub = new Subject<boolean>()
  public authChanged = this._authChangeSub.asObservable();
  Navigation :any;
  // store the URL so we can redirect after logging in
  redirectUrl: string = "";
  token: any;

  constructor(@Inject(ENV_CONFIG) private config: EnvironmentConfig, private _http: HttpClient, private _jwtHelper: JwtHelperService) {
    this.apiUrl = `${config.environment.baseUrl}`;
  }
 
  // public InvokeAsync = (route: string, body: "NavigationMenu") => {
  //   return this._http.post<AuthResponseDto>(this.createCompleteRoute(route, `${this.apiUrl}`), body).pipe(
  //     delay(1000),
  //     tap(val => this.Navigation)
  //   );
  // }

  // public forgotPassword = (route: string, body: ForgotPasswordDto) => {
  //   return this._http.post(this.createCompleteRoute(route, `${this.apiUrl}`), body);
  // }

  // public resetPassword = (route: string, body: ResetPasswordDto) => {
  //   return this._http.post(this.createCompleteRoute(route, `${this.apiUrl}`), body);
  // }

  public logout = () => {
    this.isLoggedIn = false;
    localStorage.removeItem("token");
    this.sendAuthStateChangeNotification(false);
  }
  // public confirmEmail = (route: string, token: string, email: string) => {
  //   let params = new HttpParams({ encoder: new CustomEncoder() })
  //   params = params.append('token', token);
  //   params = params.append('email', email);
  //   return this._http.get(this.createCompleteRoute(route, `${this.apiUrl}`), { params: params });
  // }
  public sendAuthStateChangeNotification = (isAuthenticated: boolean) => {
    this._authChangeSub.next(isAuthenticated);
    this.isLoggedIn = true;
  }
  public isUserAuthenticated = (): boolean => {
    this.token = localStorage.getItem("token");

    return this.token && !this._jwtHelper.isTokenExpired(this.token);
  }
  public isUserAdmin = (): boolean => {    
    const token = localStorage.getItem("token");

    if(token != null){
      return true;
    }
    else{
      return false;
    }
    //const decodedToken = this._jwtHelper.decodeToken(token);
    // if (decodedToken != null) {
   
    //   const userid = decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid'];
    //   const emailid = decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'];
    //   const role = decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']
     
    //   sessionStorage.setItem("user_id", userid);
    //   sessionStorage.setItem("email_id", emailid);
    //   sessionStorage.setItem("user_role", role);
    //   return true; //role === 'Administrator';
    // }
    // else {
    //   return false; //role === 'Administrator';
    // }
  }

  private createCompleteRoute = (route: string, envAddress: string) => {    
    return `${envAddress}${route}`;
  }
}
export interface UserForAuthenticationDto {
  Email: string;
  ProjectNo: string;
}
export interface AuthResponseDto {
  isAuthSuccessful: boolean;
  errorMessage: string;
  token: string;
}